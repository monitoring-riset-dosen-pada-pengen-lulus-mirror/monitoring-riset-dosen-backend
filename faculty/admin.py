from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth import get_user_model

from faculty.models import Lab, TopikPenelitian

User = get_user_model()


class LabAdminForm(forms.ModelForm):
    """
    Modify Lab admin form so that administrators
    could add users to this group.

    Reference:
    https://stackoverflow.com/a/39648244/13018015
    """
    users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False,
        widget=FilteredSelectMultiple('users', False)
    )

    class Meta:
        model = Lab
        exclude = []

    def __init__(self, *args, **kwargs):
        super(LabAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['users'].initial = self.instance.user_set.all()

    def save_m2m(self):
        self.instance.user_set.set(self.cleaned_data['users'])

    def save(self, *args, **kwargs):
        instance = super(LabAdminForm, self).save()
        self.save_m2m()
        return instance


class LabAdmin(admin.ModelAdmin):
    form = LabAdminForm
    filter_horizontal = ['permissions']


admin.site.register(Lab, LabAdmin)
admin.site.register(TopikPenelitian)
