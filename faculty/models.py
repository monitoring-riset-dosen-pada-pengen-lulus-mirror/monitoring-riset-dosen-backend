from django.contrib.auth.models import Group as DjangoGroup
from django.db import models


class Lab(DjangoGroup):
    deskripsi = models.TextField(blank=True)

    def __str__(self) -> str:
        return self.name


class TopikPenelitian(models.Model):
    nama_topik = models.CharField(blank=False, max_length=255)
    deskripsi = models.TextField(blank=True)

    def __str__(self) -> str:
        return self.nama_topik
