from django.urls import path

from . import views

app_name = 'faculty'
urlpatterns = [
    path('lab/', views.LabList.as_view(), name='lab_list'),
    path('lab/opsi-lab/', views.OpsiLabList.as_view(), name='opsi_lab_list'),
    path('topik/opsi-topik/', views.OpsiTopikPenelitianList.as_view(), name='opsi_topik_penelitian_list'),
]
