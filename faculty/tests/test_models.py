from django.test import TestCase

from faculty.models import Lab, TopikPenelitian


class LabModelTest(TestCase):
    def setUp(self):
        self.lab = Lab.objects.create(
            name='RSE UI',
            deskripsi='RSE UI Adalah Lab di Fasilkom UI'
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Lab
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.lab),
            'RSE UI'
        )


class TopikPenelitianTest(TestCase):
    def setUp(self):
        self.topik = TopikPenelitian.objects.create(
            nama_topik='ML',
            deskripsi='Machine Learning'
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Lab
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.topik),
            'ML'
        )
