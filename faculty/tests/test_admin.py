from django.contrib.auth.models import User
from django.test import TestCase

from faculty.admin import LabAdminForm
from faculty.models import Lab


class LabAdminTest(TestCase):
    def setUp(self):
        self.anggota_1 = User.objects.create_superuser(
            first_name="Anggota Dosen",
            last_name="Daus",
            email="anggota_1@gmail.com",
            username="anggota_1",
            password="password_anggota_1"
        )
        self.anggota_2 = User.objects.create_superuser(
            first_name="Anggota Mahasiswa",
            last_name="David",
            email="anggota_2@gmail.com",
            username="anggota_2",
            password="password_anggota_2"
        )
        self.form_data_ai = {
            'name': 'Lab Artificial Intelligence',
            'users': [self.anggota_1.id, self.anggota_2.id]
        }
        self.lab_ms = Lab.objects.create(name='Lab Microsoft')
        self.form_data_ms = {
            'name': 'Lab Microsoft',
            'deskripsi': 'Lab Microsoft adalah Lab di Fasilkom UI',
            'users': [self.anggota_1.id, self.anggota_2.id],
        }

    def test_if_form_valid(self):
        """
        Testing if the form valid when
        creating object with form_data_ai
        """
        form = LabAdminForm(data=self.form_data_ai)
        if form.is_valid():
            form.save()
        self.assertTrue(form.is_valid())

    def test_updating_form(self):
        """
        Testing if the form valid when
        updating object with form_data_2
        """
        form = LabAdminForm(
            data=self.form_data_ms,
            instance=self.lab_ms
        )
        if form.is_valid():
            form.save()
        self.assertTrue(form.is_valid())
        self.assertEqual(len(form.data['users']), 2)
