from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from rest_framework.test import APITestCase

from account.utils import get_tokens_for_user
from faculty.models import Lab


class GetAllLabTest(APITestCase):
    """Test module for GET Lab List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Anggota",
            last_name="Lab",
            email="anggota_lab@gmail.com",
            username="anggota_lab",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])

        Lab.objects.create(name='Lab 1')
        Lab.objects.create(name='Lab 2')
        Lab.objects.create(name='Lab 3')

    def test_get_lab_list_returns_the_right_amount(self):
        response = self.client.get(reverse('faculty:lab_list'))
        self.assertEqual(len(response.data), 3)
