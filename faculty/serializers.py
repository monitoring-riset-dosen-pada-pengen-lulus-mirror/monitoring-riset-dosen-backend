from rest_framework import serializers

from commons.serializers import OpsiSerializer
from faculty.models import Lab, TopikPenelitian


class LabSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lab
        fields = ['id', 'name', 'deskripsi']


class OpsiLabSerializer(OpsiSerializer):
    def get_label(self, obj):
        return obj.name

    def get_value(self, obj):
        return obj.id

    class Meta:
        model = Lab
        fields = ('label', 'value',)


class OpsiTopikPenelitianSerializer(OpsiSerializer):
    def get_label(self, obj):
        return obj.nama_topik

    def get_value(self, obj):
        return obj.id

    class Meta:
        model = TopikPenelitian
        fields = ('label', 'value',)
