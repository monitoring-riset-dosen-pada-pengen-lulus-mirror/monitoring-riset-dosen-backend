from rest_framework.generics import ListAPIView

from faculty.models import Lab, TopikPenelitian
from faculty.serializers import (
    LabSerializer, OpsiLabSerializer, OpsiTopikPenelitianSerializer,
)


class LabList(ListAPIView):
    pagination_class = None
    serializer_class = LabSerializer
    queryset = Lab.objects.all()


class OpsiLabList(ListAPIView):
    serializer_class = OpsiLabSerializer
    pagination_class = None
    search_fields = ['name']
    queryset = Lab.objects.all()


class OpsiTopikPenelitianList(ListAPIView):
    serializer_class = OpsiTopikPenelitianSerializer
    pagination_class = None
    search_fields = ['nama_topik']
    queryset = TopikPenelitian.objects.all()
