from django.urls import path

from . import views

app_name = 'infohibah'
urlpatterns = [
    path('', views.InfoHibahList.as_view(), name='hibah_list'),
]
