from io import BytesIO

from django.core.files import File
from django.db import models
from PIL import Image


def compress(image):
    im = Image.open(image)
    im_io = BytesIO()
    img_extension = image.name.split(".")[-1].upper()
    if img_extension == "JPG":
        img_extension = "JPEG"
    im.save(im_io, img_extension, quality=60)
    new_image = File(im_io, name=image.name)
    return new_image


class InfoHibah(models.Model):
    judul = models.CharField(max_length=255)
    deskripsi = models.TextField(blank=True)
    sumber = models.CharField(max_length=255, blank=True)
    tanggal_buat = models.DateTimeField(auto_now_add=True)
    tanggal_ubah = models.DateTimeField(auto_now=True)
    poster = models.ImageField(upload_to='poster-hibah/', null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.poster:
            new_image = compress(self.poster)
            self.poster = new_image
        super().save(*args, **kwargs)

    def __str__(self):
        return self.judul
