import json
from io import BytesIO

from django.contrib.auth.models import User
from django.core.files.base import File
from django.test import Client, TestCase
from django.urls import reverse
from PIL import Image
from rest_framework import status

from .models import InfoHibah
from .serializers import InfoHibahSerializer

client = Client()


class InfoHibahTest(TestCase):

    @staticmethod
    def get_image_file(name='test.jpg', ext='jpeg', size=(50, 50), color=(256, 0, 0)):
        file_obj = BytesIO()
        image = Image.new("RGB", size=size, color=color)
        image = image.convert("RGB")
        image.save(file_obj, ext)
        file_obj.seek(0)
        return File(file_obj, name=name)

    def setUp(self):
        self.sumber1 = "Sumber 1"
        self.deskripsi1 = "Deskripsi 1"
        self.url_for_reverse_function = 'infohibah:hibah_list'
        InfoHibah.objects.create(
            judul="Hibah 1", deskripsi=self.deskripsi1, sumber=self.sumber1,
            tanggal_buat="2020-01-01", tanggal_ubah="2020-01-09")
        InfoHibah.objects.create(
            judul="Hibah 2", deskripsi="Deskripsi 2", sumber="Sumber 2",
            tanggal_buat="2020-01-02", tanggal_ubah="2020-01-03")
        InfoHibah.objects.create(
            judul="Hibah 3", deskripsi="Deskripsi 3", sumber="Sumber 3",
            tanggal_buat="2020-01-03", tanggal_ubah="2020-01-05")
        InfoHibah.objects.create(
            judul="Hibah 4", deskripsi="Deskripsi 4", sumber="Sumber 4",
            tanggal_buat="2020-01-04", tanggal_ubah="2020-01-06")
        InfoHibah.objects.create(judul='Hibah 5')
        InfoHibah.objects.create(judul='Hibah 6')
        InfoHibah.objects.create(judul='Hibah 7')
        InfoHibah.objects.create(judul='Hibah 8')
        InfoHibah.objects.create(judul='Hibah 9')
        InfoHibah.objects.create(judul='Hibah 10')
        InfoHibah.objects.create(judul='Hibah 11')
        InfoHibah.objects.create(
            judul="Hibah Kemendikbud", deskripsi=self.deskripsi1, sumber=self.sumber1,
            tanggal_buat="2020-01-01", tanggal_ubah="2020-01-03")
        InfoHibah.objects.create(
            judul="Hibah Dikti", deskripsi="Deskripsi 2", sumber="Sumber 2",
            tanggal_buat="2020-01-02", tanggal_ubah="2020-01-05")
        self.username = "dummy"
        self.password = "dummy"
        User.objects.create_superuser(username=self.username, email="dummy@gmail.com", password=self.password)
        self.judul = "Hibah 1"
        self.deskripsi = self.deskripsi1
        self.sumber = self.sumber1
        self.tanggal_buat = "2020-01-09"
        self.tanggal_ubah = "2020-01-10"
        self.poster = self.get_image_file()
        self.judul_baru = "Hibah 2"
        self.deskripsi_baru = "deskripsi baru"
        self.sumber_baru = "sumber baru"
        self.valid_update_data = {
            "judul": self.judul_baru,
            "deskripsi": self.deskripsi_baru,
            "sumber": self.sumber_baru
        }

    def test_get_all_infohibah(self):
        response = client.get(reverse(self.url_for_reverse_function))
        self.assertEqual(response.data['count'], InfoHibah.objects.count())

    def test_get_next_page(self):
        response1 = client.get('%s?page=%s' % (reverse(self.url_for_reverse_function), 2))
        self.assertEqual(response1.data['results'][0]['judul'], 'Hibah 3')
        self.assertEqual(len(response1.data['results']), 3)

        response2 = client.get('%s?page_size=%s' % (reverse(self.url_for_reverse_function), 5))
        self.assertEqual(response2.data['results'][4]['judul'], 'Hibah 9')
        self.assertEqual(len(response2.data['results']), 5)

    def test_get_filtered_infohibah(self):
        response = client.get('%s?search=%s' % (reverse(self.url_for_reverse_function), 'kemen'))
        hibahs = InfoHibah.objects.filter(judul='Hibah Kemendikbud')
        serializer = InfoHibahSerializer(hibahs, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_latest_infohibah(self):
        response1 = client.get('%s?ordering=%s' %
                               (reverse(self.url_for_reverse_function),
                                'tanggal_ubah'))
        self.assertEqual(response1.data['results'][0]['judul'], 'Hibah 1')

        response2 = client.get('%s?search=%s&ordering=%s' %
                               (reverse(self.url_for_reverse_function),
                                'dik', '-tanggal_ubah'))
        self.assertEqual(response2.data['results'][1]['judul'], 'Hibah Kemendikbud')

    def test_update_infohibah_model_success(self):
        info_hibah = InfoHibah.objects.get(judul=self.judul)
        info_hibah.judul = self.judul_baru
        info_hibah.deskripsi = "deskripsi baru"
        info_hibah.sumber = "sumber baru"
        info_hibah.save()

        self.assertEqual(info_hibah.judul, self.judul_baru)
        self.assertEqual(info_hibah.deskripsi, self.deskripsi_baru)
        self.assertEqual(info_hibah.sumber, self.sumber_baru)
        self.assertNotEqual(info_hibah.judul, self.judul)
        self.assertNotEqual(info_hibah.deskripsi, self.deskripsi)
        self.assertNotEqual(info_hibah.sumber, self.sumber)
        self.assertNotEqual(info_hibah.tanggal_ubah, self.tanggal_ubah)

    def test_update_infohibah_via_admin(self):
        hibah_id = InfoHibah.objects.get(judul=self.judul).id
        client.login(username=self.username, password=self.password)
        response = client.post(
            reverse("admin:infohibah_infohibah_change", kwargs={'object_id': hibah_id}),
            data=json.dumps(self.valid_update_data),
            content_type='application/json'
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_infohibah_model_success(self):
        info_hibah = InfoHibah.objects.get(judul=self.judul)
        info_hibah.delete()
        self.assertFalse(InfoHibah.objects.filter(judul=self.judul).exists())
        self.assertEqual(InfoHibah.objects.count(), 12)

    def test_create_infohibah_success(self):
        total_infohibah_obj_before = InfoHibah.objects.count()
        InfoHibah.objects.create(
            judul=self.judul,
            deskripsi=self.deskripsi,
            sumber=self.sumber,
            tanggal_buat=self.tanggal_buat,
            tanggal_ubah=self.tanggal_ubah,
            poster=self.poster
        )
        self.assertEqual(InfoHibah.objects.count(),
                         total_infohibah_obj_before + 1)
