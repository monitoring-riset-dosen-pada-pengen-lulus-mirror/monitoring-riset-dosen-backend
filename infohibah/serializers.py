from rest_framework import serializers

from .models import InfoHibah


class InfoHibahSerializer(serializers.ModelSerializer):
    class Meta:
        model = InfoHibah
        fields = ('id', 'judul', 'deskripsi', 'sumber',
                  'tanggal_buat', 'tanggal_ubah', 'poster')
