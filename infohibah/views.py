from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from .models import InfoHibah
from .serializers import InfoHibahSerializer


class InfoHibahList(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = InfoHibahSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['judul']
    ordering_fields = ['tanggal_ubah']
    queryset = InfoHibah.objects.all().order_by('-tanggal_ubah')
