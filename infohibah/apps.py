from django.apps import AppConfig


class InfoHibahConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infohibah'
