import json

from django.db.models import Q


class FilterProdukParamMixin:
    """
    Mixin class that overrides ListAPIView get_queryset method
    Allowing the API consumer to pass 'tahun[]' array param
    """

    def get_queryset(self):
        years = self.request.query_params.getlist('tahun[]')
        pencipta_query = self.request.query_params.getlist('pencipta[]')
        pencipta = []
        for i in pencipta_query:
            item = json.loads(i)
            pencipta.append(item["value"])

        result_list = self.queryset
        if years:
            result_list = result_list.filter(tahun__in=years)
        if pencipta_query:
            result_list = result_list.filter(pencipta__uuid__in=pencipta)

        return result_list


class FilterKegiatanParamMixin:
    """
    Mixin class that overrides ListAPIView get_queryset method
    Allowing the API consumer to pass 'tahun[]' array param and 'peneliti[]' array of Item
    """

    def get_queryset(self):
        years = self.request.query_params.getlist('tahun[]')
        peneliti_query = self.request.query_params.getlist('peneliti[]')
        peneliti = []
        for i in peneliti_query:
            item = json.loads(i)
            peneliti.append(item["value"])

        result_list = self.queryset
        if years:
            result_list = result_list.filter(tahun__in=years)
        if peneliti_query:
            result_list = result_list.filter(Q(anggota_peneliti__uuid__in=peneliti)
                                             | Q(peneliti_utama__uuid__in=peneliti)).distinct()
        return result_list
