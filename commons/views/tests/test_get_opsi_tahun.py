from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

from account.utils import get_tokens_for_user
from kegiatan.models import Hibah
from produk.models import Paten


class GetOpsiTahunTest(TestCase):
    """Test module for opsi_tahun view"""

    def setUp(self) -> None:
        self.user = User.objects.create_superuser(
            first_name="Asdos Tahun",
            last_name="X",
            email="opsi_tahun@gmail.com",
            username="opsi_tahun",
            password="pswrd1234"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])

        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah Opsi Tahun',
            tahun=2023,
        )
        Paten.objects.create(
            judul_ciptaan='Paten Opsi Tahun',
            nomor_agenda='1',
            hibah=self.hibah,
            tahun=2023,
            jenis_paten=Paten.JenisPaten.PATEN,
            keterangan=Paten.KeteranganPaten.TERDAFTAR,
        )

    def test_get_opsi_tahun_returns_the_right_result(self):
        response = self.client.get(reverse(
            'commons:opsi_tahun', kwargs={'model_name': 'paten'}
        ))
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], 2023)

    def test_get_opsi_tahun_returns_400_bad_request(self):
        response = self.client.get(reverse(
            'commons:opsi_tahun', kwargs={'model_name': 'dummy'}
        ))
        self.assertEqual(response.status_code, 400)
