from http import HTTPStatus

from rest_framework.decorators import api_view
from rest_framework.response import Response

from commons.utils.resolve_model import resolve_model


@api_view(['GET'])
def opsi_tahun(_, model_name):
    try:
        model = resolve_model(model_name)
        year_choices = list(model.objects.values_list(model.get_tahun_attr(), flat=True).distinct())
        return Response(year_choices, status=HTTPStatus.OK)
    except KeyError:
        return Response({}, status=HTTPStatus.BAD_REQUEST)
