from kegiatan.models import Hibah
from produk.models import HKI, Paten


def resolve_model(model_name: str):
    """Resolve models based on url param"""
    model_dct = {
        'paten': Paten,
        'hki': HKI,
        'hibah': Hibah,
    }
    return model_dct[model_name]
