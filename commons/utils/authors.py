def add_et_al_to_authors(m2m_authors, principal_investigator=None):
    """
    Add 'dkk' suffix to M2M Author/Pencipta/Peneliti

    Parameters:
        m2m_authors (str): For pencipta/anggota_peneliti
        principal_investigator (str or None): For Hibah.peneliti_utama

    Returns:
        author_with_et_al (str)
    """
    all_authors = m2m_authors.all()
    num_of_authors = len(all_authors) + 1 if principal_investigator else 0
    is_author_more_than_one = num_of_authors > 1

    author = all_authors[0] if len(all_authors) else 'Unknown'
    etc = ', dkk' if is_author_more_than_one else ''

    return f'{principal_investigator or author}{etc}'
