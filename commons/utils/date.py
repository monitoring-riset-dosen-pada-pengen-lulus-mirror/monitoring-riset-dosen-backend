from django.utils import timezone


def get_current_year():
    """Return current year based on timezone format"""
    return timezone.now().year


def year_choices():
    """Generate year choices/enum for Hibah models' year attribute"""
    return [(r, r) for r in range(2005, get_current_year() + 2)]
