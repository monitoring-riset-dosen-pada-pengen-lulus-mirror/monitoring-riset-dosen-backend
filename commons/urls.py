from django.urls import path

from .views.get_opsi_tahun import opsi_tahun

# /api/commons/
app_name = 'commons'
commons_urls = [
    path('opsi-tahun/<str:model_name>/', opsi_tahun, name='opsi_tahun'),
]

urlpatterns = []
urlpatterns += commons_urls
