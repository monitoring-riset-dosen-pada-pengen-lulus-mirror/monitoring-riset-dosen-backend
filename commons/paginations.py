from django.core.paginator import EmptyPage
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):

        next_page_number = None
        prev_page_number = None

        try:
            next_page_number = self.page.next_page_number()
        except EmptyPage:
            pass

        try:
            prev_page_number = self.page.previous_page_number()
        except EmptyPage:
            pass

        return Response({
            'next': next_page_number,
            'previous': prev_page_number,
            'count': self.page.paginator.count,
            'results': data
        })
