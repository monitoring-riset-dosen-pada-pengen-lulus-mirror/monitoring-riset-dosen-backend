from commons.mixins import FilterProdukParamMixin


class ProdukMixin(FilterProdukParamMixin):
    search_fields = ['judul_ciptaan']
    ordering_fields = ['tahun']
