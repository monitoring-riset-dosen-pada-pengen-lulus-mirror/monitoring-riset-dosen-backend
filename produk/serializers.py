from datetime import datetime

from rest_framework import serializers

from account.models import Profil
from author.models import Pembuat
from commons.utils.date import get_current_year
from kegiatan.models import Hibah

from .models import HKI, Paten, Produk


class HibahReadOnlySerializer(serializers.ModelSerializer):
    class Meta:
        model = Hibah
        fields = ['id', 'judul_penelitian']


class PenciptaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profil
        fields = '__all__'

    def to_representation(self, instance):
        return instance.nama_lengkap


class PatenSerializer(serializers.ModelSerializer):
    hibah = HibahReadOnlySerializer()
    pencipta = PenciptaSerializer(many=True, read_only=True)

    class Meta:
        model = Paten
        fields = '__all__'


class HKISerializer(serializers.ModelSerializer):
    hibah = HibahReadOnlySerializer(required=False, allow_null=True)
    pencipta = PenciptaSerializer(many=True, read_only=True)

    class Meta:
        model = HKI
        fields = '__all__'


class OutputHibahSerializer(serializers.ModelSerializer):

    class Meta:
        model = Produk
        fields = ['id', 'judul_ciptaan', 'jenis']


class ExcelHKISerializer(serializers.Serializer):
    tahun = serializers.IntegerField(default=get_current_year())
    jenis_hki = serializers.CharField(max_length=255, allow_blank=True)
    tanggal_rilis = serializers.CharField(allow_blank=True, allow_null=True)
    pencipta = serializers.CharField(max_length=255, allow_blank=True)
    judul_ciptaan = serializers.CharField(max_length=255, allow_blank=True)
    keterangan = serializers.CharField(max_length=255, allow_blank=True)

    def create(self, validated_data):
        hki_attrs = self.extract_hki(validated_data)
        pencipta = hki_attrs.pop('pencipta')

        instance = HKI.objects.create(**hki_attrs)
        if pencipta:
            instance.pencipta.set(pencipta)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        hki_attrs = self.extract_hki(validated_data)
        pencipta = hki_attrs.pop('pencipta')
        HKI.objects.filter(id=instance.id).update(**hki_attrs)

        instance = HKI.objects.get(id=instance.id)
        if pencipta:
            instance.pencipta.set(pencipta)
        instance.save()
        return instance

    def extract_hki(self, validated_data):
        hki_attrs = validated_data.copy()

        # tahun
        if validated_data['tahun']:
            hki_attrs['tahun'] = int(validated_data['tahun'])

        # jenis hki
        jenis_hki = {
            'BASIS DATA': HKI.JenisHKI.BASIS_DATA,
            'BUKU': HKI.JenisHKI.BUKU,
            'KARYA TULIS': HKI.JenisHKI.KARYA_TULIS,
            'PROGRAM KOMPUTER': HKI.JenisHKI.PROGRAM_KOMPUTER,
            'PATEN': HKI.JenisHKI.PATEN,
        }
        jenis_hki_data = validated_data['jenis_hki']
        if jenis_hki_data:
            if 'basis data' in jenis_hki_data.lower():
                hki_attrs['jenis_hki'] = jenis_hki['BASIS DATA']
            elif 'buku' in jenis_hki_data.lower():
                hki_attrs['jenis_hki'] = jenis_hki['BUKU']
            elif 'karya tulis' in jenis_hki_data.lower():
                hki_attrs['jenis_hki'] = jenis_hki['KARYA TULIS']
            elif 'program komputer' in jenis_hki_data.lower():
                hki_attrs['jenis_hki'] = jenis_hki['PROGRAM KOMPUTER']
            elif 'paten' in jenis_hki_data.lower():
                hki_attrs['jenis_hki'] = jenis_hki['PATEN']
            else:
                hki_attrs['jenis_hki'] = ''
        else:
            hki_attrs['jenis_hki'] = ''

        # keterangan
        keterangan = {
            'TERCATAT': HKI.KeteranganHKI.TERCATAT,
            'TERDAFTAR': HKI.KeteranganHKI.TERDAFTAR,
        }
        keterangan_data = validated_data['keterangan']
        if keterangan_data:
            if not keterangan_data.isalpha():
                try:
                    hki_attrs['no_registrasi_hak_cipta'] = int(
                        [int(s) for s in keterangan_data.split() if s.isdigit() and len(s) > 2][0])
                except IndexError:
                    hki_attrs['no_registrasi_hak_cipta'] = ''
                finally:
                    try:
                        hki_attrs['keterangan'] = 'No. Registrasi Hak Cipta: %d' % int(float(keterangan_data))
                    except ValueError:
                        hki_attrs['keterangan'] = ''

            if type(keterangan_data) is str and 'tercatat' in keterangan_data.lower():
                hki_attrs['keterangan'] = keterangan['TERCATAT']
            elif type(keterangan_data) is str and 'terdaftar' in keterangan_data.lower():
                hki_attrs['keterangan'] = keterangan['TERDAFTAR']
        else:
            hki_attrs['keterangan'] = ''

        # pencipta
        if validated_data['pencipta']:
            raw_list_anggota = validated_data['pencipta'].split(';')
            list_anggota_pencipta = []
            for anggota in raw_list_anggota:
                nama_anggota = anggota.strip()
                anggota_pencipta, _ = Pembuat.objects.get_or_create(
                    nama_lengkap__iexact=nama_anggota, defaults={"nama_lengkap": nama_anggota})
                list_anggota_pencipta.append(anggota_pencipta)
            hki_attrs['pencipta'] = list_anggota_pencipta
        else:
            hki_attrs['pencipta'] = ''

        # tanggal rilis
        if validated_data['tanggal_rilis']:
            hki_attrs['tanggal_rilis'] = datetime.strptime(validated_data['tanggal_rilis'], '%Y-%m-%dT%H:%M')
        else:
            hki_attrs['tanggal_rilis'] = None

        return hki_attrs


class ExcelPatenSerializer(serializers.Serializer):
    tahun = serializers.IntegerField(default=get_current_year())
    jenis_paten = serializers.CharField(max_length=255, allow_blank=True)
    tanggal_permohonan = serializers.CharField(allow_blank=True, allow_null=True)
    nomor = serializers.IntegerField(allow_null=True)
    nomor_agenda = serializers.CharField(max_length=255, allow_blank=True)
    pencipta = serializers.CharField(max_length=255, allow_blank=True)
    judul_ciptaan = serializers.CharField(max_length=255, allow_blank=True)
    keterangan = serializers.CharField(max_length=255, allow_blank=True)

    def create(self, validated_data):
        paten_attrs = self.extract_paten(validated_data)
        pencipta = paten_attrs.pop('pencipta')

        instance = Paten.objects.create(**paten_attrs)
        if pencipta:
            instance.pencipta.set(pencipta)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        paten_attrs = self.extract_paten(validated_data)
        pencipta = paten_attrs.pop('pencipta')
        Paten.objects.filter(id=instance.id).update(**paten_attrs)

        instance = Paten.objects.get(id=instance.id)
        if pencipta:
            instance.pencipta.set(pencipta)
        instance.save()
        return instance

    def extract_paten(self, validated_data):
        paten_attrs = validated_data.copy()

        # tahun
        if validated_data['tahun']:
            paten_attrs['tahun'] = int(validated_data['tahun'])

        # nomor
        if validated_data['nomor']:
            paten_attrs['nomor'] = int(validated_data['nomor'])
        else:
            paten_attrs['nomor'] = None

        # jenis paten
        jenis_paten = {
            'PATEN': Paten.JenisPaten.PATEN,
            'PATEN SEDERHANA': Paten.JenisPaten.PATEN_SEDERHANA,
        }
        jenis_paten_data = validated_data['jenis_paten']
        if jenis_paten_data:
            if jenis_paten_data.upper() == 'PATEN':
                paten_attrs['jenis_paten'] = jenis_paten['PATEN']
            elif jenis_paten_data.upper() == 'PATEN SEDERHANA':
                paten_attrs['jenis_paten'] = jenis_paten['PATEN SEDERHANA']
            else:
                paten_attrs['jenis_paten'] = ''
        else:
            paten_attrs['jenis_paten'] = ''

        # keterangan
        keterangan = {
            'TERDAFTAR': Paten.KeteranganPaten.TERDAFTAR,
            'BELUM TERDAFTAR': Paten.KeteranganPaten.BELUM_TERDAFTAR,
        }
        keterangan_data = validated_data['keterangan']
        if keterangan_data:
            if type(keterangan_data) is str and keterangan_data.lower() == 'belum terdaftar':
                paten_attrs['keterangan'] = keterangan['BELUM TERDAFTAR']
            elif type(keterangan_data) is str and keterangan_data.lower() == 'terdaftar':
                paten_attrs['keterangan'] = keterangan['TERDAFTAR']
        else:
            paten_attrs['keterangan'] = ''

        # pencipta
        if validated_data['pencipta']:
            raw_list_anggota = validated_data['pencipta'].split(';')
            list_anggota_pencipta = []
            for anggota in raw_list_anggota:
                nama_anggota = anggota.strip()
                anggota_pencipta, _ = Pembuat.objects.get_or_create(
                    nama_lengkap__iexact=nama_anggota, defaults={"nama_lengkap": nama_anggota})
                list_anggota_pencipta.append(anggota_pencipta)
            paten_attrs['pencipta'] = list_anggota_pencipta
        else:
            paten_attrs['pencipta'] = ''

        # tanggal permohonan
        if validated_data['tanggal_permohonan']:
            paten_attrs['tanggal_permohonan'] = datetime.strptime(
                validated_data['tanggal_permohonan'], '%Y-%m-%dT%H:%M')
        else:
            paten_attrs['tanggal_permohonan'] = None

        return paten_attrs
