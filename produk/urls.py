from django.urls import path

from . import views

app_name = 'outputhibah'
urlpatterns = [
    path('paten/', views.PatenList.as_view(), name='paten_list'),
    path("paten/<uuid:id>", views.PatenDetail.as_view(), name="paten"),
    path('hki/', views.HKIList.as_view(), name='hki_list'),
    path('hki/<uuid:id>', views.HKIDetail.as_view(), name="hki"),
    path('upload-data/', views.UploadProduk.as_view(), name='upload_produk'),
]
