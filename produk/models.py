import uuid
from datetime import datetime

from django.db import models

from author.models import Pembuat
from commons.utils.authors import add_et_al_to_authors
from commons.utils.date import get_current_year, year_choices
from kegiatan.models import Hibah


class Produk(models.Model):
    class JenisOutputHibah(models.TextChoices):
        HKI = "Hak Kekayaan Intelektual"
        JURNAL = "Jurnal"
        PATEN = "Paten"

    class Meta:
        ordering = ['-tahun']

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    judul_ciptaan = models.CharField(max_length=255, blank=True)
    tanggal_permohonan = models.DateTimeField(blank=True, null=True)
    hibah = models.ForeignKey(
        Hibah,
        related_name='output',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    link = models.URLField(blank=True, null=True)
    jenis = models.CharField(
        max_length=255,
        blank=True,
        choices=JenisOutputHibah.choices,
        default=JenisOutputHibah.JURNAL
    )
    tahun = models.IntegerField(choices=year_choices(), default=get_current_year())

    @staticmethod
    def has(judul_ciptaan: str, tanggal_permohonan: datetime, tahun: int, jenis: str) -> bool:
        '''
        Mengecek apakah OutputHibah sudah memiliki data dengan
        atribut yang diminta
        @param judul_ciptaan:
        @param tanggal_permohonan:
        @param tahun:
        @param jenis: pilih berdasarkan class JenisOutputHibah
        @return: boolean
        '''
        filtered_produk_objects = Produk.objects.filter(hibah__tahun=tahun,
                                                        tanggal_permohonan=tanggal_permohonan,
                                                        jenis=jenis)
        for produk_object in filtered_produk_objects.all():
            if judul_ciptaan.strip().lower() == str(produk_object.judul_ciptaan).strip().lower():
                return True
        return False

    @staticmethod
    def get_tahun_attr():
        return 'tahun'

    def __str__(self) -> str:
        return f'{self.jenis.title()} | {self.hibah.judul_penelitian}'


class Paten(Produk):
    class JenisPaten(models.TextChoices):
        PATEN = "Paten"
        PATEN_SEDERHANA = "Paten Sederhana"

    class KeteranganPaten(models.TextChoices):
        TERDAFTAR = "Terdaftar"
        BELUM_TERDAFTAR = "Belum Terdaftar"

    jenis_paten = models.CharField(
        max_length=255,
        blank=True,
        choices=JenisPaten.choices,
        default=JenisPaten.PATEN
    )
    nomor = models.IntegerField(blank=True, null=True)
    nomor_agenda = models.CharField(max_length=255, blank=True)
    pencipta = models.ManyToManyField(
        Pembuat,
        related_name='paten_pencipta',
        blank=True
    )
    keterangan = models.CharField(
        max_length=255,
        blank=True,
        choices=KeteranganPaten.choices,
        default=KeteranganPaten.TERDAFTAR,
    )
    no_registrasi_paten = models.CharField(max_length=255, blank=True)
    tanggal_rilis = models.DateTimeField(blank=True, null=True)

    @staticmethod
    def get_jumlah_paten():
        result = {"paten": {}, "paten_sederhana": {}}
        patens = Paten.objects.all()
        current_year = get_current_year()
        lower_bound_year = current_year - 10
        for year in range(current_year, lower_bound_year, -1):
            current_paten = patens.filter(tahun=year)
            result["paten"][year] = \
                current_paten.filter(jenis_paten=Paten.JenisPaten.PATEN).count()
            result["paten_sederhana"][year] = \
                current_paten.filter(jenis_paten=Paten.JenisPaten.PATEN_SEDERHANA).count()
        return result

    def __str__(self) -> str:
        author_with_et_al = add_et_al_to_authors(self.pencipta)
        judul = self.judul_ciptaan or self.hibah.judul_penelitian
        return f'{author_with_et_al} | {judul}'


class HKI(Produk):
    class JenisHKI(models.TextChoices):
        BASIS_DATA = "Hak Cipta Basis Data"
        BUKU = "Hak Cipta Buku"
        KARYA_TULIS = "Hak Cipta Karya Tulis"
        PROGRAM_KOMPUTER = "Hak Cipta Program Komputer"
        PATEN = "Paten"

    class KeteranganHKI(models.TextChoices):
        TERCATAT = "Tercatat"
        TERDAFTAR = "Terdaftar"

    jenis_hki = models.CharField(
        max_length=255,
        blank=True,
        choices=JenisHKI.choices,
        default=JenisHKI.PROGRAM_KOMPUTER
    )
    pencipta = models.ManyToManyField(
        Pembuat,
        related_name='hki_pencipta',
        blank=True
    )
    keterangan = models.CharField(max_length=255, blank=True, choices=KeteranganHKI.choices)
    no_registrasi_hak_cipta = models.CharField(max_length=255, blank=True)
    tanggal_rilis = models.DateTimeField(blank=True, null=True)

    @staticmethod
    def get_jumlah_hki():
        result = {"basis_data": {},
                  "paten": {},
                  "karya_tulis": {},
                  "program_komputer": {},
                  "buku": {}}
        hkis = HKI.objects.all()
        current_year = get_current_year()
        lower_bound_year = current_year - 10
        for year in range(current_year, lower_bound_year, -1):
            current_hki = hkis.filter(tahun=year)
            result["basis_data"][year] = \
                current_hki.filter(jenis_hki=HKI.JenisHKI.BASIS_DATA).count()
            result["paten"][year] = \
                current_hki.filter(jenis_hki=HKI.JenisHKI.PATEN).count()
            result["karya_tulis"][year] = \
                current_hki.filter(jenis_hki=HKI.JenisHKI.KARYA_TULIS).count()
            result["program_komputer"][year] = \
                current_hki.filter(jenis_hki=HKI.JenisHKI.PROGRAM_KOMPUTER).count()
            result["buku"][year] = \
                current_hki.filter(jenis_hki=HKI.JenisHKI.BUKU).count()
        return result

    def __str__(self) -> str:
        author_with_et_al = add_et_al_to_authors(self.pencipta)
        judul = self.judul_ciptaan or self.hibah.judul_penelitian
        return f'{author_with_et_al} | {judul}'
