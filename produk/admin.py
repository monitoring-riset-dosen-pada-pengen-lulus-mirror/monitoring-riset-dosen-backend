from django.contrib import admin, messages
from django.db.models import Max, Min

from commons.utils.authors import add_et_al_to_authors
from produk.models import HKI, Paten, Produk

admin.site.site_header = 'SIHIBAH Admin Dasboard'


class YearFilter(admin.SimpleListFilter):
    template = 'admin/year_filter.html'
    title = 'tahun'
    parameter_name = 'tahun'
    tahun_dari = ""
    tahun_hingga = ""
    ERROR_MSG_TAHUN_DARI_GREATER_THAN_TAHUN_HINGGA = \
        'Input "Hingga" tidak boleh lebih kecil dari input "Dari".'
    WARNING_MSG_TAHUN_HINGGA_LOWER_THAN_MIN_TAHUN_DARI = \
        'Input "Hingga" lebih kecil dari tahun terlawas.'
    WARNING_MSG_TAHUN_DARI_GREATER_THAN_MAX_TAHUN_HINGGA = \
        'Input "Dari" lebih besar dari tahun terbaru.'

    def lookups(self, request, model_admin):
        return ("None", "None"), ("None", "None")

    def queryset(self, request, queryset):
        if self.value() is None:
            return None
        self.tahun_dari = self.value().split('-')[0]
        self.tahun_hingga = self.value().split('-')[1]
        if len(self.tahun_hingga) < 1 and len(self.tahun_dari) < 1:
            return None
        if len(self.tahun_hingga) < 1:
            self.tahun_hingga = str(queryset.values('tahun').aggregate(Max('tahun'))['tahun__max'])
            if int(self.tahun_hingga) < int(self.tahun_dari):
                self.tahun_dari = self.tahun_hingga
                messages.warning(request,
                                 self.WARNING_MSG_TAHUN_DARI_GREATER_THAN_MAX_TAHUN_HINGGA)
        if len(self.tahun_dari) < 1:
            self.tahun_dari = str(queryset.values('tahun').aggregate(Min('tahun'))['tahun__min'])
            if int(self.tahun_dari) > int(self.tahun_hingga):
                self.tahun_hingga = self.tahun_dari
                messages.warning(request,
                                 self.WARNING_MSG_TAHUN_HINGGA_LOWER_THAN_MIN_TAHUN_DARI)
        tahun_dari = int(self.tahun_dari)
        tahun_hingga = int(self.tahun_hingga)
        if tahun_hingga < tahun_dari:
            return messages.error(request,
                                  self.ERROR_MSG_TAHUN_DARI_GREATER_THAN_TAHUN_HINGGA)
        return queryset.filter(
            tahun__gte=int(tahun_dari),
            tahun__lte=int(tahun_hingga)
        )


class OutputHibahAdmin(admin.ModelAdmin):
    list_display = ('tahun', 'author', 'judul_penelitian')
    list_filter = (YearFilter,)
    search_fields = ('hibah__anggota_peneliti__nama_lengkap',
                     'hibah__peneliti_utama__nama_lengkap',
                     'hibah__judul_penelitian')

    def author(self, output_hibah_obj):
        return add_et_al_to_authors(
            output_hibah_obj.hibah.anggota_peneliti,
            output_hibah_obj.hibah.peneliti_utama
        )

    def judul_penelitian(self, output_hibah_obj):
        return output_hibah_obj.hibah.judul_penelitian


class HKIAdmin(OutputHibahAdmin):
    list_display = ('tahun', 'author', 'judul_ciptaan', 'judul_penelitian')
    search_fields = ('hibah__anggota_peneliti__nama_lengkap',
                     'hibah__peneliti_utama__nama_lengkap',
                     'hibah__judul_penelitian',
                     'judul_ciptaan')


class PatenAdmin(OutputHibahAdmin):
    list_display = ('tahun', 'author', 'judul_ciptaan', 'judul_penelitian')
    search_fields = ('hibah__anggota_peneliti__nama_lengkap',
                     'hibah__peneliti_utama__nama_lengkap',
                     'hibah__judul_penelitian',
                     'judul_ciptaan')


admin.site.register(Produk, OutputHibahAdmin)
admin.site.register(HKI, HKIAdmin)
admin.site.register(Paten, PatenAdmin)
