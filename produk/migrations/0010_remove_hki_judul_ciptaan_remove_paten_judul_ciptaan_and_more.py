# Generated by Django 4.0.2 on 2022-04-27 02:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produk',  '0009_rename_pencipta1_hki_pencipta_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hki',
            name='judul_ciptaan',
        ),
        migrations.RemoveField(
            model_name='paten',
            name='judul_ciptaan',
        ),
        migrations.AddField(
            model_name='outputhibah',
            name='judul_ciptaan',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
