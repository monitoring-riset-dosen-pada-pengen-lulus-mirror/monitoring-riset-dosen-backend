# Generated by Django 4.0.2 on 2022-05-17 14:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0026_alter_kegiatan_tahun'),
        ('produk', '0017_alter_hki_produk_ptr_alter_paten_produk_ptr'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produk',
            name='hibah',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='output', to='kegiatan.hibah'),
        ),
        migrations.AlterField(
            model_name='produk',
            name='tahun',
            field=models.IntegerField(choices=[(2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023)], default=2022),
        ),
    ]
