import datetime

from django.test import TestCase

from kegiatan.models import Hibah
from produk.models import HKI, Paten, Produk


class OutputHibahModelTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Penelitian OutputHibah',
            tahun=2022
        )
        self.output_hibah = Produk.objects.create(
            hibah=self.hibah,
            jenis=Produk.JenisOutputHibah.JURNAL.value,
            tanggal_permohonan=datetime.datetime(2020, 3, 1, 2, 3, 4, 5)
        )
        self.judul_penelitian = "Hibah 1"
        self.hibah2 = Hibah.objects.create(
            judul_penelitian=self.judul_penelitian,
            tahun=2022
        )
        self.date1 = datetime.datetime(2020, 2, 1, 2, 3, 4, 5)
        self.judul_ciptaan = "testing"
        self.output_hibah2 = Produk.objects.create(
            judul_ciptaan=self.judul_ciptaan,
            hibah=self.hibah2,
            jenis=Produk.JenisOutputHibah.PATEN.value,
            tanggal_permohonan=self.date1
        )

    def test_string_representation(self):
        """
        Testing the case whether or not OutputHibah
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.output_hibah),
            'Jurnal | Penelitian OutputHibah'
        )

    def test_static_method_has_return_true(self):
        result = Produk.has(self.judul_ciptaan, self.date1, 2022,
                            Produk.JenisOutputHibah.PATEN.value)
        expected = True
        self.assertEqual(expected, result)

    def test_static_method_has_return_false(self):
        result = Produk.has(self.hibah2.judul_penelitian, self.date1, 2022,
                            Produk.JenisOutputHibah.HKI.value)
        expected = False
        self.assertEqual(expected, result)

    def test_static_method_get_tahun_attr_return_tahun(self):
        self.assertEqual("tahun", Produk.get_tahun_attr())


class PatenModelTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Penelitian Paten',
            tahun=2022
        )
        self.paten = Paten.objects.create(
            hibah=self.hibah,
            jenis=Produk.JenisOutputHibah.PATEN.value
        )

    def test_string_representation(self):
        self.assertEqual(
            str(self.paten),
            'Unknown | Penelitian Paten'
        )


class HKIModelTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah untuk HKI',
            tahun=2022
        )
        self.hki = HKI.objects.create(
            hibah=self.hibah,
            jenis=Produk.JenisOutputHibah.HKI.value,
            jenis_hki=HKI.JenisHKI.KARYA_TULIS.value
        )

    def test_the_right_hki_type_is_returned(self):
        self.assertEqual(
            self.hki.jenis_hki,
            'Hak Cipta Karya Tulis'
        )

    def test_string_representation(self):
        self.assertEqual(
            str(self.hki),
            'Unknown | Hibah untuk HKI'
        )
