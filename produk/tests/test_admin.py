from django.contrib.admin.sites import AdminSite
from django.test import RequestFactory, TestCase

from author.models import Pembuat
from faculty.admin import User
from kegiatan.models import Hibah
from produk.admin import OutputHibahAdmin, YearFilter
from produk.models import HKI, Paten, Produk


class HKIAdminTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah untuk HKI',
            tahun=2022
        )
        self.hki = HKI.objects.create(
            hibah=self.hibah,
            tahun=2022,
            jenis_hki=HKI.JenisHKI.KARYA_TULIS.value,
            judul_ciptaan="Kampus Merdeka 2022",
            keterangan="TERDAFTAR"
        )

    def test_update_hki_via_admin(self):
        hki = HKI.objects.get(id=self.hki.id)
        hki.judul_ciptaan = 'E-Hajj Improvement'
        hki.jenis_hki = HKI.JenisHKI.PROGRAM_KOMPUTER.value
        hki.save()

        self.assertEqual(hki.judul_ciptaan, 'E-Hajj Improvement')
        self.assertEqual(hki.jenis_hki, 'Hak Cipta Program Komputer')

    def test_delete_hki_via_admin(self):
        amount_hki_obj_before = HKI.objects.count()
        hki = HKI.objects.get(id=self.hki.id)
        hki.delete()
        self.assertEqual(HKI.objects.count(), amount_hki_obj_before - 1)


class PatenAdminTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian="Hibah untuk Paten",
            tahun=2022
        )
        self.paten = Paten.objects.create(
            hibah=self.hibah,
            tahun=2022,
            jenis_paten=Paten.JenisPaten.PATEN.value,
            judul_ciptaan="Touch Screen",
            keterangan=Paten.KeteranganPaten.TERDAFTAR.value
        )

    def test_update_paten_via_admin(self):
        paten = Paten.objects.get(id=self.paten.id)
        paten.judul_ciptaan = "Metode Geser Jari pada Galeri"
        paten.jenis_paten = Paten.JenisPaten.PATEN_SEDERHANA.value
        paten.keterangan = Paten.KeteranganPaten.BELUM_TERDAFTAR.value
        paten.save()

        self.assertEqual(paten.judul_ciptaan, "Metode Geser Jari pada Galeri")
        self.assertEqual(paten.jenis_paten, "Paten Sederhana")
        self.assertEqual(paten.keterangan, "Belum Terdaftar")

    def test_delete_paten_via_admin(self):
        amount_paten_obj_before = Paten.objects.count()
        paten = Paten.objects.get(id=self.paten.id)
        paten.delete()
        self.assertEqual(Paten.objects.count(), amount_paten_obj_before - 1)


class AdminConfiguration(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Dosen",
            last_name="A",
            email="dosen_a@gmail.com",
            username="dosen_a",
            password="password"
        )
        self.user_dosen = Pembuat.objects.create(
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}',
        )
        self.hibah = Hibah.objects.create(
            judul_penelitian="Hibah untuk Paten",
            tahun=2022,
            peneliti_utama=self.user_dosen
        )
        self.outputhibah = Produk.objects.create(
            hibah=self.hibah,
            tahun=2022,
            jenis=Produk.JenisOutputHibah.PATEN.value,
        )


class OutputhibahAdminTest(AdminConfiguration):
    def test_author_method_in_OutputHibahAdminClass(self):
        outputhibah = Produk.objects.all()[0]
        outputhibah_admin = OutputHibahAdmin(model=Produk, admin_site=AdminSite())
        result = outputhibah_admin.author(outputhibah)
        self.assertEqual(result, "Dosen A")

    def test_judul_penelitian_method_in_OutputHibahAdminClass(self):
        outputhibah = Produk.objects.all()[0]
        outputhibah_admin = OutputHibahAdmin(model=Produk, admin_site=AdminSite())
        result = outputhibah_admin.judul_penelitian(outputhibah)
        self.assertEqual(result, "Hibah untuk Paten")


class DummyStorage:
    """
    dummy message-store to test the api methods
    """

    def __init__(self):
        self.store = []

    def add(self, level, message, extra_tags=''):
        self.store.append(message)


class YearFilterTest(AdminConfiguration):
    def setUp(self) -> None:
        super().setUp()

    def test_lookups_method_return_tuple(self):
        year_filter = YearFilter(None, {'tahun': '2010-2020'}, Produk, OutputHibahAdmin)
        result = year_filter.lookups(None, OutputHibahAdmin)
        self.assertEqual(tuple, type(result))

    def test_queryset_method_with_params_tahun_2010_2022_return_1(self):
        year_filter = YearFilter(None, {'tahun': '2010-2022'}, Produk, OutputHibahAdmin)
        result = year_filter.queryset(None, Produk.objects.all())
        self.assertEqual(1, len(result))

    def test_queryset_method_with_params_tahun_2010_2022_return_0(self):
        year_filter = YearFilter(None, {'tahun': '2010-2020'}, Produk, OutputHibahAdmin)
        result = year_filter.queryset(None, Produk.objects.all())
        self.assertEqual(0, len(result))

    def test_queryset_method_with_params_empty_return_none(self):
        year_filter = YearFilter(None, {}, Produk, OutputHibahAdmin)
        result = year_filter.queryset(None, Produk.objects.all())
        self.assertEqual(None, result)

    def test_queryset_method_with_params_2010_dash_return_1(self):
        year_filter = YearFilter(None, {"tahun": "2010-"}, Produk, OutputHibahAdmin)
        request = RequestFactory().request()
        storage = DummyStorage()
        request._messages = storage
        result = year_filter.queryset(request, Produk.objects.all())
        self.assertEqual(1, len(result))

    def test_queryset_method_with_params_dash_2022_return_1(self):
        year_filter = YearFilter(None, {"tahun": "-2022"}, Produk, OutputHibahAdmin)
        request = RequestFactory().request()
        storage = DummyStorage()
        request._messages = storage
        result = year_filter.queryset(request, Produk.objects.all())
        self.assertEqual(1, len(result))

    def test_queryset_method_with_params_just_dash_return_none(self):
        year_filter = YearFilter(None, {"tahun": "-"}, Produk, OutputHibahAdmin)
        result = year_filter.queryset(None, Produk.objects.all())
        self.assertEqual(None, result)

    def test_queryset_method_with_params_dash_2000_return_warning_msg(self):
        year_filter = YearFilter(None, {"tahun": "-2000"}, Produk, OutputHibahAdmin)
        request = RequestFactory().request()
        storage = DummyStorage()
        request._messages = storage
        year_filter.queryset(request, Produk.objects.all())
        expected = year_filter.WARNING_MSG_TAHUN_HINGGA_LOWER_THAN_MIN_TAHUN_DARI
        result = storage.store
        self.assertIn(expected, result)

    def test_queryset_method_with_params_2030_dash_return_warning_msg(self):
        year_filter = YearFilter(None, {"tahun": "2030-"}, Produk, OutputHibahAdmin)
        request = RequestFactory().request()
        storage = DummyStorage()
        request._messages = storage
        year_filter.queryset(request, Produk.objects.all())
        expected = year_filter.WARNING_MSG_TAHUN_DARI_GREATER_THAN_MAX_TAHUN_HINGGA
        result = storage.store
        self.assertIn(expected, result)

    def test_queryset_method_with_params_2020_2010_return_error_msg(self):
        year_filter = YearFilter(None, {"tahun": "2020-2010"}, Produk, OutputHibahAdmin)
        request = RequestFactory().request()
        storage = DummyStorage()
        request._messages = storage
        year_filter.queryset(request, Produk.objects.all())
        expected = year_filter.ERROR_MSG_TAHUN_DARI_GREATER_THAN_TAHUN_HINGGA
        result = storage.store
        self.assertIn(expected, result)
