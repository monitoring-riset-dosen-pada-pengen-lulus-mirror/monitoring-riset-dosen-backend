from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from account.utils import get_tokens_for_user
from author.models import Pembuat
from kegiatan.models import Hibah
from produk.models import HKI, Paten
from produk.serializers import HKISerializer, PatenSerializer
from utils.create_bearer import create_bearer

primary_paten_name = "Paten 1"
url_for_reverse_function_paten = 'outputhibah:paten_list'
url_for_reverse_function_hki = 'outputhibah:hki_list'


class GetAllPatenTest(TestCase):
    """Test module for GET Paten List API"""

    def setUp(self):
        self.paten2 = 'Paten 2'
        self.paten3 = 'Paten 3'
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah Ilmu Komputer',
            tahun=2022,
        )
        Paten.objects.create(
            judul_ciptaan=primary_paten_name,
            no_registrasi_paten='1',
            nomor_agenda='1',
            hibah=self.hibah,
            tahun=2022
        )
        Paten.objects.create(
            judul_ciptaan=self.paten2,
            no_registrasi_paten='12',
            nomor_agenda='2',
            hibah=self.hibah,
            tahun=2022
        )
        Paten.objects.create(
            judul_ciptaan=self.paten3,
            no_registrasi_paten='123',
            nomor_agenda='3',
            hibah=self.hibah,
            tahun=2022
        )

    def test_get_paten_list_returns_the_right_amount(self):
        response = self.client.get(reverse(url_for_reverse_function_paten))
        self.assertEqual(response.data['count'], 3)

    def test_get_paten_list_next_page(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function_paten)}?page={2}&page_size={1}")
        self.assertEqual(response.data['results'][0]['judul_ciptaan'], self.paten2)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_paten_list_with_custom_page_size(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function_paten)}?page_size={5}")
        self.assertEqual(response.data['results'][2]['judul_ciptaan'], self.paten3)
        self.assertEqual(len(response.data['results']), 3)


class GetFilteredPatenTest(TestCase):
    """Test module for GET Filtered Paten"""

    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah Ilmu Alam',
            tahun=2022,
        )
        self.paten2 = 'Paten 2'
        Paten.objects.create(
            judul_ciptaan=primary_paten_name,
            no_registrasi_paten='1234',
            nomor_agenda='1',
            hibah=self.hibah,
            tahun=2022,
            jenis_paten=Paten.JenisPaten.PATEN,
            keterangan=Paten.KeteranganPaten.TERDAFTAR,
        )
        self.user = User.objects.create_superuser(
            first_name="Dosen Hibah",
            last_name="A",
            email="dosen_hibah@gmail.com",
            username="dosen_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.user_dosen = Pembuat.objects.create(
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}'
        )
        self.paten = Paten.objects.create(
            judul_ciptaan=self.paten2,
            no_registrasi_paten='12341',
            nomor_agenda='2',
            hibah=self.hibah,
            tahun=2022,
            jenis_paten=Paten.JenisPaten.PATEN_SEDERHANA,
            keterangan=Paten.KeteranganPaten.BELUM_TERDAFTAR
        )
        self.paten.pencipta.add(self.user_dosen.uuid)
        self.paten.save()

    def test_get_paten_by_search_query(self):
        response = self.client.get(
            '%s?search=%s&pencipta=%s' % (
                reverse(url_for_reverse_function_paten),
                primary_paten_name,
                'Dosen Hibah',
            ),
        )
        hibahs = Paten.objects.filter(judul_ciptaan=primary_paten_name)
        serializer = PatenSerializer(hibahs, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_filtered_paten(self):
        response1 = self.client.get(
            '%s?jenis_paten=%s&keterangan=%s&tahun[]=%s' % (
                reverse(url_for_reverse_function_paten),
                'Paten',
                'Terdaftar',
                '2022',
            ),
        )
        self.assertEqual(len(response1.data['results']), 1)
        self.assertEqual(response1.data['results'][0]['judul_ciptaan'], primary_paten_name)

        response2 = self.client.get(
            '%s?jenis_paten=%s&keterangan=%s' % (
                reverse(url_for_reverse_function_paten),
                'Paten Sederhana',
                'Belum Terdaftar',
            ),
        )
        self.assertEqual(len(response2.data['results']), 1)
        self.assertEqual(response2.data['results'][0]['judul_ciptaan'], self.paten2)


class GetAllHKITest(TestCase):
    """Test module for GET HKI List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Staf HKI",
            last_name="A",
            email="staf_hki@gmail.com",
            username="staf_hki",
            password="dumbdumb"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah HKI',
            tahun=2022,
        )
        HKI.objects.create(
            judul_ciptaan='HKI 1',
            hibah=self.hibah,
            tahun=2022
        )
        HKI.objects.create(no_registrasi_hak_cipta='1',
                           judul_ciptaan='HKI 2',
                           hibah=self.hibah,
                           tahun=2022
                           )
        HKI.objects.create(no_registrasi_hak_cipta='2',
                           judul_ciptaan='HKI 3',
                           hibah=self.hibah,
                           tahun=2022
                           )

    def test_get_hki_list_returns_the_right_amount(self):
        response = self.client.get(reverse(url_for_reverse_function_hki))
        self.assertEqual(response.data['count'], 3)

    def test_get_hki_list_next_page(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function_hki)}?page={3}&page_size={1}")
        self.assertEqual(response.data['results'][0]['judul_ciptaan'], 'HKI 3')
        self.assertEqual(len(response.data['results']), 1)

    def test_get_hki_list_with_custom_page_size(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function_hki)}?page_size={5}")
        self.assertEqual(response.data['results'][1]['judul_ciptaan'], 'HKI 2')
        self.assertEqual(len(response.data['results']), 3)


class GetFilteredHKITest(TestCase):
    """Test module for GET Filtered HKI"""

    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah untuk HKI',
            tahun=2022,
        )
        HKI.objects.create(no_registrasi_hak_cipta='23',
                           judul_ciptaan='HKI 1',
                           hibah=self.hibah,
                           tahun=2022,
                           jenis_hki=HKI.JenisHKI.BUKU,
                           keterangan=HKI.KeteranganHKI.TERCATAT
                           )
        self.user = User.objects.create_superuser(
            first_name="Dosen HKI",
            last_name="A",
            email="dosen_hki@gmail.com",
            username="dosen_hki",
            password="dumbdumb"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.user_dosen = Pembuat.objects.create(
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}'
        )
        self.hki2 = HKI.objects.create(no_registrasi_hak_cipta='234',
                                       judul_ciptaan='HKI 2',
                                       hibah=self.hibah,
                                       tahun=2019,
                                       jenis_hki=HKI.JenisHKI.KARYA_TULIS,
                                       keterangan=HKI.KeteranganHKI.TERDAFTAR
                                       )
        self.hki2.pencipta.add(self.user_dosen.uuid)
        self.hki2.save()

    def test_get_hki_by_search_query(self):
        response = self.client.get(
            '%s?search=%s' % (
                reverse(url_for_reverse_function_hki),
                'HKI 2',
            ),
        )
        hkis = HKI.objects.filter(judul_ciptaan='HKI 2')
        serializer = HKISerializer(hkis, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_filtered_hki(self):
        response1 = self.client.get(
            '%s?jenis_hki=%s&keterangan=%s' % (
                reverse(url_for_reverse_function_hki),
                'Hak Cipta Buku',
                'Tercatat'
            ),
        )
        self.assertEqual(len(response1.data['results']), 1)
        self.assertEqual(response1.data['results'][0]['judul_ciptaan'], 'HKI 1')

        response2 = self.client.get(
            '%s?jenis_hki=%s&keterangan=%s&tahun[]=%s' % (
                reverse(url_for_reverse_function_hki),
                'Hak Cipta Karya Tulis',
                'Terdaftar',
                '2019'
            ),
        )
        self.assertEqual(len(response2.data['results']), 1)
        self.assertEqual(response2.data['results'][0]['judul_ciptaan'], 'HKI 2')


class TestPatenDetailAPIView(APITestCase):
    """Test Paten Detail API view"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah Ilmu Komputer',
            tahun=2022,
        )
        self.paten2 = "Paten 2"
        Paten.objects.create(
            judul_ciptaan=primary_paten_name,
            no_registrasi_paten='1',
            nomor_agenda='1',
            hibah=self.hibah,
            tahun=2022
        )
        Paten.objects.create(
            judul_ciptaan=self.paten2,
            no_registrasi_paten='2',
            nomor_agenda='2',
            hibah=self.hibah,
            tahun=2022
        )
        Paten.objects.create(
            judul_ciptaan='Paten 3',
            no_registrasi_paten='3', nomor_agenda='3',
            hibah=self.hibah,
            tahun=2022
        )
        self.url_for_reverse_function = 'outputhibah:paten_list'

    def test_retrieve_one_item(self):
        response = self.client.get(reverse(self.url_for_reverse_function))
        res = self.client.get(
            reverse("outputhibah:paten", kwargs={'id': response.data['results'][0]['id']})
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['judul_ciptaan'], primary_paten_name)

    def test_delete_item(self):
        response = self.client.get(reverse(self.url_for_reverse_function))
        self.client.delete(
            reverse("outputhibah:paten", kwargs={'id': response.data['results'][0]['id']})
        )
        self.assertEqual(Paten.objects.all().count(), 2)


class TestHKIDetailAPIView(APITestCase):
    """Test HKI Detail API view"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Staf HKI",
            last_name="A",
            email="staf_hki@gmail.com",
            username="staf_hki",
            password="dumbdumb"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah HKI',
            tahun=2022,
        )
        HKI.objects.create(no_registrasi_hak_cipta='2342',
                           judul_ciptaan='HKI 1',
                           hibah=self.hibah,
                           tahun=2022
                           )
        HKI.objects.create(no_registrasi_hak_cipta='23423',
                           judul_ciptaan='HKI 2',
                           hibah=self.hibah,
                           tahun=2022
                           )
        HKI.objects.create(no_registrasi_hak_cipta='234232',
                           judul_ciptaan='HKI 3',
                           hibah=self.hibah,
                           tahun=2022
                           )

    def test_retrieve_one_item(self):
        response = self.client.get(reverse(url_for_reverse_function_hki))
        res = self.client.get(
            reverse("outputhibah:hki", kwargs={'id': response.data['results'][0]['id']})
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['judul_ciptaan'], 'HKI 1')

    def test_delete_item(self):
        response = self.client.get(reverse(url_for_reverse_function_hki))
        self.client.delete(
            reverse("outputhibah:hki", kwargs={'id': response.data['results'][0]['id']})
        )
        self.assertEqual(HKI.objects.all().count(), 2)
