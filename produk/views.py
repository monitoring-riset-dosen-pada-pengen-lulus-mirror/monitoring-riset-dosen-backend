from django.utils.datastructures import MultiValueDictKeyError
from pyexcel_xls import get_data as xls_get
from pyexcel_xlsx import get_data as xlsx_get
from rest_framework import status
from rest_framework.generics import (
    CreateAPIView, ListAPIView, RetrieveDestroyAPIView,
)
from rest_framework.response import Response

from commons.utils.date import get_current_year
from kegiatan.models import Kegiatan
from produk.mixins import ProdukMixin

from .models import HKI, Paten
from .serializers import (
    ExcelHKISerializer, ExcelPatenSerializer, HKISerializer, PatenSerializer,
)


class PatenList(ProdukMixin, ListAPIView):
    serializer_class = PatenSerializer
    filterset_fields = ['jenis_paten', 'keterangan']
    queryset = Paten.objects.all()


class HKIList(ProdukMixin, ListAPIView):
    serializer_class = HKISerializer
    filterset_fields = ['jenis_hki', 'keterangan']
    queryset = HKI.objects.all()


class PatenDetail(RetrieveDestroyAPIView):
    serializer_class = PatenSerializer
    lookup_field = 'id'
    queryset = Paten.objects.all()


class HKIDetail(RetrieveDestroyAPIView):
    serializer_class = HKISerializer
    lookup_field = 'id'
    queryset = HKI.objects.all()


class UploadProduk(CreateAPIView):
    def get_item_or_default(self, list_query, index, default):
        return list_query[index] if index < len(list_query) else default

    def post(self, request, format=None):
        try:
            excel_file = request.FILES.get('uploaded_file')
        except MultiValueDictKeyError:
            return Response({'message': 'File failed to upload'}, status=status.HTTP_400_BAD_REQUEST)

        if (str(excel_file).split('.')[-1] == 'xls'):
            data = xls_get(excel_file)
        elif (str(excel_file).split('.')[-1] == 'xlsx'):
            data = xlsx_get(excel_file)
        else:
            return Response({'message': 'File type not supported'}, status=status.HTTP_400_BAD_REQUEST)

        hki_excel = data['HKI']
        paten_excel = data['Paten']

        if hki_excel and paten_excel:
            if (len(hki_excel) > 2):
                hkis = hki_excel[1:]
                for hki in hkis:
                    if len(hki) == 0:
                        break
                    else:
                        judul_ciptaan = self.get_item_or_default(hki, 4, "")
                        data = {
                            "judul_ciptaan": judul_ciptaan,
                            "tahun": self.get_item_or_default(hki, 0, get_current_year()),
                            "jenis_hki": self.get_item_or_default(hki, 1, "").strip(),
                            "tanggal_rilis": self.get_item_or_default(hki, 2, ""),
                            "pencipta": self.get_item_or_default(hki, 3, None),
                            "keterangan": str(self.get_item_or_default(hki, 5, "")),
                        }

                        query_existing_kegiatan = Kegiatan.objects.filter(judul_penelitian__iexact=judul_ciptaan)
                        if query_existing_kegiatan.exists():
                            instance = query_existing_kegiatan[0]
                            serializer = ExcelHKISerializer(instance=instance, data=data)
                        else:
                            serializer = ExcelHKISerializer(data=data)

                        if (serializer.is_valid()):
                            serializer.save()
                        else:
                            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            if (len(paten_excel) > 2):
                patens = paten_excel[1:]
                for paten in patens:
                    if len(paten) == 0:
                        break
                    else:
                        judul_ciptaan = self.get_item_or_default(paten, 6, "")
                        data = {
                            "judul_ciptaan": judul_ciptaan,
                            "tahun": self.get_item_or_default(paten, 0, get_current_year()),
                            "jenis_paten": self.get_item_or_default(paten, 1, "").strip(),
                            "nomor": self.get_item_or_default(paten, 2, ""),
                            "nomor_agenda": self.get_item_or_default(paten, 3, "").strip(),
                            "tanggal_permohonan": self.get_item_or_default(paten, 4, ""),
                            "pencipta": self.get_item_or_default(paten, 5, None),
                            "keterangan": str(self.get_item_or_default(paten, 7, "").strip()),
                        }

                        query_existing_kegiatan = Kegiatan.objects.filter(judul_penelitian__iexact=judul_ciptaan)
                        if query_existing_kegiatan.exists():
                            instance = query_existing_kegiatan[0]
                            serializer = ExcelPatenSerializer(instance=instance, data=data)
                        else:
                            serializer = ExcelPatenSerializer(data=data)

                        if (serializer.is_valid()):
                            serializer.save()
                        else:
                            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response({'message': 'Successfully uploaded'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'No HKI/Paten data found'}, status=status.HTTP_400_BAD_REQUEST)
