from drf_yasg import openapi

from commons.utils.date import year_choices
from kegiatan.models import Hibah, PemberiDana

status_usulan_params = openapi.Parameter(
    "status_usulan", openapi.IN_QUERY, type=openapi.TYPE_STRING, enum=Hibah.StatusUsulan.values
)

year_params = openapi.Parameter(
    "tahun", openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="List of tahun", enum=year_choices()
)

tipe_pendanaan_params = openapi.Parameter(
    "tipe_pendanaan", openapi.IN_QUERY, type=openapi.TYPE_STRING, enum=PemberiDana.TipePendanaan.values
)

pengmas_list_fields = ['id', 'judul_penelitian', 'status_usulan', 'tahun', 'is_non_hibah',
                       'peneliti_utama', 'lab', 'pemberi_dana', 'anggota_peneliti']

riset_list_fields = ['id', 'judul_penelitian', 'status_usulan', 'tahun', 'is_non_hibah',
                     'peneliti_utama', 'lab', 'pemberi_dana', 'anggota_peneliti']

kegiatan_create_excludes = ['tipe_kegiatan']

hibah_create_excludes = ['is_non_hibah']
