
from rest_framework import serializers

from author.models import Pembuat
from commons.serializers import DynamicFieldsModelSerializer, OpsiSerializer
from commons.utils.date import get_current_year
from faculty.models import Lab, TopikPenelitian
from faculty.serializers import LabSerializer
from kegiatan.models import Hibah, Kegiatan, PemberiDana
from produk.serializers import OutputHibahSerializer


class PrincipalInvestigatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pembuat
        fields = '__all__'

    def to_representation(self, instance):
        return instance.nama_lengkap


class AnggotaPenelitiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pembuat
        fields = '__all__'

    def to_representation(self, instance):
        return instance.nama_lengkap


class PemberiDanaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PemberiDana
        fields = '__all__'


class OpsiPemberiDanaSerializer(OpsiSerializer):
    def get_label(self, obj):
        return obj.nama

    def get_value(self, obj):
        return obj.id

    class Meta:
        model = PemberiDana
        fields = ('label', 'value',)


class TopikPenelitianSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopikPenelitian
        fields = '__all__'


class KegiatanSerializer(DynamicFieldsModelSerializer):
    peneliti_utama = PrincipalInvestigatorSerializer()
    anggota_peneliti = AnggotaPenelitiSerializer(many=True, required=False, allow_null=True)
    lab = LabSerializer(required=False, allow_null=True)
    output = OutputHibahSerializer(many=True, read_only=True)
    topik_penelitian = TopikPenelitianSerializer(many=True)

    class Meta:
        model = Kegiatan
        fields = '__all__'

    def update(self, instance, validated_data):
        peneliti_utama_data = validated_data.pop('peneliti_utama')
        lab_data = validated_data.pop('lab')
        anggota_peneliti_data = validated_data.pop('anggota_peneliti')
        topik_penelitian_data = validated_data.pop('topik_penelitian')

        instance.peneliti_utama = Pembuat.objects.get_or_create(**peneliti_utama_data)[0]

        if lab_data is not None:
            instance.lab = Lab.objects.get_or_create(**lab_data)[0]

        anggota_peneliti = []
        for nama_pembuat in anggota_peneliti_data:
            peneliti = Pembuat.objects.get_or_create(**nama_pembuat)[0]
            anggota_peneliti.append(peneliti)
        instance.anggota_peneliti.set(anggota_peneliti)

        topik_penelitian = []
        for nama_topik in topik_penelitian_data:
            topik = TopikPenelitian.objects.get_or_create(**nama_topik)[0]
            topik_penelitian.append(topik)
        instance.topik_penelitian.set(topik_penelitian)

        for field, value in validated_data.items():
            setattr(instance, field, value)

        instance.save()
        return instance


class HibahSerializer(KegiatanSerializer):
    pemberi_dana = PemberiDanaSerializer(required=False, allow_null=True)

    class Meta:
        model = Hibah
        fields = '__all__'

    def update(self, instance, validated_data):
        pemberi_dana_data = validated_data.pop('pemberi_dana')

        if pemberi_dana_data is not None:
            instance.pemberi_dana = PemberiDana.objects.get_or_create(**pemberi_dana_data)[0]

        super().update(instance, validated_data)
        instance.save()
        return instance


class KegiatanNonHibahSerializer(serializers.ModelSerializer):
    peneliti_utama = serializers.PrimaryKeyRelatedField(queryset=Pembuat.objects.all())
    anggota_peneliti = serializers.PrimaryKeyRelatedField(many=True, allow_empty=True, queryset=Pembuat.objects.all())
    lab = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=Lab.objects.all())

    class Meta:
        model = Kegiatan
        fields = '__all__'


class KegiatanHibahSerializer(serializers.ModelSerializer):
    peneliti_utama = serializers.PrimaryKeyRelatedField(queryset=Pembuat.objects.all())
    anggota_peneliti = serializers.PrimaryKeyRelatedField(many=True, allow_empty=True, queryset=Pembuat.objects.all())
    lab = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=Lab.objects.all())
    pemberi_dana = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=PemberiDana.objects.all())

    class Meta:
        model = Hibah
        fields = '__all__'


class ExcelKegiatanSerializer(serializers.Serializer):
    judul_penelitian = serializers.CharField(max_length=255)
    tahun = serializers.IntegerField(default=get_current_year())
    tipe_kegiatan = serializers.CharField(max_length=255, allow_blank=True)
    peneliti_utama = serializers.CharField(max_length=255, allow_blank=True, allow_null=True)
    lab = serializers.CharField(max_length=255, allow_blank=True)
    anggota_peneliti = serializers.CharField(max_length=255, allow_blank=True)
    no_nota_dinas = serializers.CharField(max_length=255, allow_blank=True)
    no_kontrak_hibah = serializers.CharField(max_length=255, allow_blank=True)
    skema_pembiayaan = serializers.CharField(max_length=255, allow_blank=True)
    nilai_hibah_nasional = serializers.CharField(allow_blank=True)
    nilai_hibah_internasional = serializers.CharField(allow_blank=True)
    pemberi_dana = serializers.CharField(max_length=255, allow_blank=True, allow_null=True)
    status_usulan = serializers.CharField(max_length=255, allow_blank=True)

    def create(self, validated_data):
        hibah_attrs = self.extract_hibah(validated_data)
        anggota_peneliti = hibah_attrs.pop('anggota_peneliti')

        instance = Hibah.objects.create(**hibah_attrs)
        if anggota_peneliti:
            instance.anggota_peneliti.set(anggota_peneliti)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        hibah_attrs = self.extract_hibah(validated_data)
        anggota_peneliti = hibah_attrs.pop('anggota_peneliti')
        Hibah.objects.filter(id=instance.id).update(**hibah_attrs)

        instance = Hibah.objects.get(id=instance.id)
        if anggota_peneliti:
            instance.anggota_peneliti.set(anggota_peneliti)
        instance.save()
        return instance

    def extract_hibah(self, validated_data):
        hibah_attrs = validated_data.copy()

        # tipe kegiatan
        tipe_kegiatan = {
            "riset": Kegiatan.TipeKegiatan.RISET,
            "pengabdian masyarakat": Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT
        }
        if validated_data['tipe_kegiatan']:
            hibah_attrs['tipe_kegiatan'] = tipe_kegiatan[validated_data['tipe_kegiatan'].lower()]

        # peneliti_utama
        if validated_data['peneliti_utama']:
            nama_lab = validated_data['peneliti_utama']
            peneliti_utama, _ = Pembuat.objects.get_or_create(
                nama_lengkap__iexact=nama_lab, defaults={"nama_lengkap": nama_lab})
            hibah_attrs['peneliti_utama'] = peneliti_utama
        else:
            hibah_attrs['peneliti_utama'] = None

        # lab
        if validated_data['lab']:
            nama_lab = validated_data['lab']
            lab, _ = Lab.objects.get_or_create(name__iexact=nama_lab, defaults={"name": nama_lab})
            hibah_attrs['lab'] = lab
        else:
            hibah_attrs['lab'] = None

        # anggota_peneliti
        if validated_data['anggota_peneliti']:
            raw_list_anggota = validated_data['anggota_peneliti'].split(';')
            list_anggota_peneliti = []
            for anggota in raw_list_anggota:
                nama_anggota = anggota.strip()
                anggota_peneliti, _ = Pembuat.objects.get_or_create(
                    nama_lengkap__iexact=nama_anggota, defaults={"nama_lengkap": nama_anggota})
                list_anggota_peneliti.append(anggota_peneliti)
            hibah_attrs['anggota_peneliti'] = list_anggota_peneliti
        else:
            hibah_attrs['anggota_peneliti'] = None

        # nilai_hibah_nasional
        if validated_data['nilai_hibah_nasional']:
            nilai_hibah_nasional = validated_data['nilai_hibah_nasional']
            hibah_attrs['nilai_hibah_nasional'] = int(nilai_hibah_nasional)
        else:
            hibah_attrs['nilai_hibah_nasional'] = 0

        # nilai_hibah_internasional
        if validated_data['nilai_hibah_internasional']:
            nilai_hibah_internasional = validated_data['nilai_hibah_internasional']
            hibah_attrs['nilai_hibah_internasional'] = int(nilai_hibah_internasional)
        else:
            hibah_attrs['nilai_hibah_internasional'] = 0

        # pemberi_dana
        if validated_data['pemberi_dana']:
            nama_lab = validated_data['pemberi_dana']
            pemberi_dana, _ = PemberiDana.objects.get_or_create(
                nama__iexact=nama_lab, defaults={"nama": nama_lab})
            hibah_attrs['pemberi_dana'] = pemberi_dana
        else:
            hibah_attrs['pemberi_dana'] = None

        # status_usulan
        tipe_usulan = {
            "baru": Hibah.StatusUsulan.BARU,
            "lanjutan": Hibah.StatusUsulan.LANJUTAN
        }
        if validated_data['status_usulan']:
            nama_status_usulan = validated_data['status_usulan']
            status_usulan = tipe_usulan[nama_status_usulan.lower()]
            hibah_attrs['status_usulan'] = status_usulan

        return hibah_attrs
