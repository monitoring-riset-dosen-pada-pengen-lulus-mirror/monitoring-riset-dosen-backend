from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from account.models import Dosen
from account.utils import get_tokens_for_user
from faculty.models import Lab
from kegiatan.models import Hibah, PemberiDana
from kegiatan.serializers import HibahSerializer, KegiatanSerializer
from kegiatan.swagger_params import pengmas_list_fields, riset_list_fields
from produk.tests.test_views import create_bearer

url_for_reverse_function = 'hibah:hibah_list'


class GetAllHibahTest(APITestCase):
    """Test module for GET Hibah List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        Hibah.objects.create(
            judul_penelitian='Hibah 1',
            tahun=2022
        )
        Hibah.objects.create(
            judul_penelitian='Hibah 2',
            tahun=2022
        )
        Hibah.objects.create(
            judul_penelitian='Hibah 3',
            tahun=2022
        )

    def test_get_hibah_list_returns_the_right_amount(self):
        response = self.client.get(reverse(url_for_reverse_function))
        self.assertEqual(response.data['count'], 3)

    def test_get_hibah_list_next_page(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function)}?page={2}&page_size={1}")
        self.assertEqual(len(response.data['results']), 1)

    def test_get_hibah_list_with_custom_page_size(self):
        response = self.client.get(
            f"{reverse(url_for_reverse_function)}?page_size={5}")
        self.assertEqual(len(response.data['results']), 3)


class GetFilteredHibahTest(APITestCase):
    """Test module for GET Filtered Hibah"""

    def setUp(self):
        self.judul_penelitian = 'Hibah Ilmu Alam'
        self.hibah1 = Hibah.objects.create(
            judul_penelitian=self.judul_penelitian,
            tahun=2022,
        )
        self.user_peneliti_utama = User.objects.create_superuser(
            first_name="Principal Investigator Hibah",
            last_name="A",
            email="peneliti_utama_hibah@gmail.com",
            username="peneliti_utama_hibah",
            password="pasuworudo"
        )
        self.user_peneliti = User.objects.create_superuser(
            first_name="Peneliti Hibah",
            last_name="B",
            email="peneliti_hibah@gmail.com",
            username="peneliti_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user_peneliti_utama)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.peneliti_utama = Dosen.objects.create(
            user=self.user_peneliti_utama,
            nama_lengkap=f'{self.user_peneliti_utama.first_name} {self.user_peneliti_utama.last_name}',
            email="dosen_hibah@gmail.com",
        )
        self.peneliti = Dosen.objects.create(
            user=self.user_peneliti,
            nama_lengkap=f'{self.user_peneliti.first_name} {self.user_peneliti.last_name}',
            email="peneliti_hibah@gmail.com",
        )
        self.lab = Lab.objects.create(
            name='Reliable Software Engineering'
        )
        self.pemberi_dana = PemberiDana.objects.create(
            nama='KEMENRISTEKDIKTI'
        )
        self.hibah2 = Hibah.objects.create(
            judul_penelitian='Hibah Ilmu Antariksa',
            tahun=2022,
            lab=self.lab,
            pemberi_dana=self.pemberi_dana,
        )

    def test_get_hibah_by_search_query(self):
        response = self.client.get(
            '%s?search=%s' % (
                reverse(url_for_reverse_function),
                self.judul_penelitian,
            ),
        )
        hibahs = Hibah.objects.filter(judul_penelitian=self.judul_penelitian)
        serializer = HibahSerializer(hibahs, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_filtered_hibah(self):
        response = self.client.get(
            '%s?lab=%s&pemberi_dana=%s&tahun[]=%s' % (
                reverse(url_for_reverse_function),
                self.lab.id,
                self.pemberi_dana.id,
                '2022',
            ),
        )
        self.assertEqual(len(response.data['results']), 1)


class GetAllRisetTest(APITestCase):
    """Test module for GET Riset List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Riset",
            last_name="B",
            email="asdos_riset@gmail.com",
            username="asdos_riset",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        Hibah.objects.create(
            judul_penelitian='Riset 1',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Riset 2',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Riset 3',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )

    def test_get_riset_list_returns_the_right_amount_of_riset(self):
        response = self.client.get(reverse('hibah:riset_list'))
        self.assertEqual(response.data['count'], 2)

    def test_get_riset_list_next_page(self):
        response = self.client.get(
            f"{reverse('hibah:riset_list')}?page={2}&page_size={1}")
        self.assertEqual(len(response.data['results']), 1)

    def test_get_hibah_list_with_custom_page_size(self):
        response = self.client.get(
            f"{reverse('hibah:riset_list')}?page_size={5}")
        self.assertEqual(len(response.data['results']), 2)


class GetFilteredRisetTest(APITestCase):
    """Test module for GET Filtered Hibah"""

    def setUp(self):
        self.hibah1 = Hibah.objects.create(
            judul_penelitian='Riset Ilmu Alam',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET,
        )
        self.user_peneliti_utama = User.objects.create_superuser(
            first_name="Principal Investigator riset",
            last_name="A",
            email="peneliti_utama_riset@gmail.com",
            username="peneliti_utama_riset",
            password="pasuworudo"
        )
        self.user_peneliti = User.objects.create_superuser(
            first_name="Peneliti riset",
            last_name="B",
            email="peneliti_riset@gmail.com",
            username="peneliti_riset",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user_peneliti_utama)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        self.peneliti_utama = Dosen.objects.create(
            user=self.user_peneliti_utama,
            nama_lengkap=f'{self.user_peneliti_utama.first_name} {self.user_peneliti_utama.last_name}',
            email="dosen_riset@gmail.com",
        )
        self.peneliti = Dosen.objects.create(
            user=self.user_peneliti,
            nama_lengkap=f'{self.user_peneliti.first_name} {self.user_peneliti.last_name}',
            email="peneliti_riset@gmail.com",
        )
        self.lab = Lab.objects.create(
            name='Reliable Software Engineering'
        )
        self.pemberi_dana = PemberiDana.objects.create(
            nama='KEMENRISTEKDIKTI'
        )
        self.riset2 = Hibah.objects.create(
            judul_penelitian='riset Ilmu Antariksa',
            tahun=2022,
            lab=self.lab,
            pemberi_dana=self.pemberi_dana,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET,
        )

    def test_get_riset_by_search_query(self):
        response = self.client.get(
            '%s?search=%s' % (
                reverse('hibah:riset_list'),
                'riset Ilmu Alam',
            ),
        )
        riset = Hibah.objects.filter(judul_penelitian='Riset Ilmu Alam')
        serializer = KegiatanSerializer(riset, many=True, fields=riset_list_fields)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_filtered_riset(self):
        response = self.client.get(
            '%s?lab=%s&pemberi_dana=%s&tahun[]=%s&tipe_pendanaan=%s' % (
                reverse('hibah:riset_list'),
                self.lab.id,
                self.pemberi_dana.id,
                '2022',
                'Internal',
            ),
        )
        self.assertEqual(len(response.data['results']), 1)


class GetAllPemberiDanaTest(APITestCase):
    """Test module for GET Pemberi Dana List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Anggota",
            last_name="Hibah",
            email="anggota_hibah@gmail.com",
            username="anggota_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))

        PemberiDana.objects.create(nama='Pemberi Dana 1')
        PemberiDana.objects.create(nama='Pemberi Dana 2')
        PemberiDana.objects.create(nama='Pemberi Dana 3')

    def test_get_lab_list_returns_the_right_amount(self):
        response = self.client.get(reverse('hibah:pemberi_dana_list'))
        self.assertEqual(len(response.data), 3)


class GetAllPengmasTest(APITestCase):
    """Test module for GET Hibah List API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        Hibah.objects.create(
            judul_penelitian='Pengmas 1',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )
        Hibah.objects.create(
            judul_penelitian='Pengmas 2',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )
        Hibah.objects.create(
            judul_penelitian='Pengmas 3',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )

    def test_get_pengmas_list_returns_the_right_amount(self):
        response = self.client.get(reverse('hibah:pengabdian_masyarakat_list'))
        self.assertEqual(response.data['count'], 3)

    def test_get_pengmas_list_next_page(self):
        response = self.client.get(
            f"{reverse('hibah:pengabdian_masyarakat_list')}?page={2}&page_size={1}")
        self.assertEqual(len(response.data['results']), 1)

    def test_get_pengmas_list_with_custom_page_size(self):
        response = self.client.get(
            f"{reverse('hibah:pengabdian_masyarakat_list')}?page_size={5}")
        self.assertEqual(len(response.data['results']), 3)


class GetFilteredPengmasTest(APITestCase):
    """Test module for GET Filtered Hibah"""

    def setUp(self):
        self.hibah1 = Hibah.objects.create(
            judul_penelitian='Pengmas Ilmu Alam',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT,
        )
        self.user_peneliti_utama = User.objects.create_superuser(
            first_name="Principal Investigator pengmas",
            last_name="A",
            email="peneliti_utama_pengmas@gmail.com",
            username="peneliti_utama_pengmas",
            password="pasuworudo"
        )
        self.user_peneliti = User.objects.create_superuser(
            first_name="Peneliti pengmas",
            last_name="B",
            email="peneliti_pengmas@gmail.com",
            username="peneliti_pengmas",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user_peneliti_utama)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        self.peneliti_utama = Dosen.objects.create(
            user=self.user_peneliti_utama,
            nama_lengkap=f'{self.user_peneliti_utama.first_name} {self.user_peneliti_utama.last_name}',
            email="dosen_pengmas@gmail.com",
        )
        self.peneliti = Dosen.objects.create(
            user=self.user_peneliti,
            nama_lengkap=f'{self.user_peneliti.first_name} {self.user_peneliti.last_name}',
            email="peneliti_pengmas@gmail.com",
        )
        self.lab = Lab.objects.create(
            name='Reliable Software Engineering'
        )
        self.pemberi_dana = PemberiDana.objects.create(
            nama='KEMENRISTEKDIKTI'
        )
        self.pengmas2 = Hibah.objects.create(
            judul_penelitian='pengmas Ilmu Antariksa',
            tahun=2022,
            lab=self.lab,
            pemberi_dana=self.pemberi_dana,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT,
        )

    def test_get_pengmas_by_search_query(self):
        response = self.client.get(
            '%s?search=%s' % (
                reverse('hibah:pengabdian_masyarakat_list'),
                'pengmas Ilmu Alam',
            ),
        )
        pengmas = Hibah.objects.filter(judul_penelitian='Pengmas Ilmu Alam')
        serializer = KegiatanSerializer(pengmas, many=True, fields=pengmas_list_fields)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_filtered_pengmas(self):
        response = self.client.get(
            '%s?lab=%s&pemberi_dana=%s&tahun[]=%s&tipe_pendanaan=%s' % (
                reverse('hibah:pengabdian_masyarakat_list'),
                self.lab.id,
                self.pemberi_dana.id,
                '2022',
                'Internal',
            ),
        )
        self.assertEqual(len(response.data['results']), 1)


class TestHibahDetailAPIView(APITestCase):
    """Test Hibah Detail API view"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        Hibah.objects.create(
            judul_penelitian='Hibah A',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah B',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah C',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )

    def test_retrieve_one_item_hibah(self):
        hibahs = self.client.get(reverse('hibah:hibah_list'))
        response = self.client.get(
            reverse("hibah:hibah_detail", kwargs={'id': hibahs.data['results'][0]['id']})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['judul_penelitian'], 'Hibah A')


class TestRisetDetailAPIView(APITestCase):
    """Test Riset Detail API view"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Penelitian",
            last_name="B",
            email="asdos_penelitian@gmail.com",
            username="asdos_penelitian"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        Hibah.objects.create(
            judul_penelitian='Hibah a',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah b',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah c',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )

    def test_retrieve_one_riset(self):
        hibahs = self.client.get(reverse('hibah:hibah_list'))
        response = self.client.get(
            reverse('hibah:riset_detail', kwargs={'id': hibahs.data['results'][0]['id']})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['judul_penelitian'], 'Hibah a')

    def test_retrieve_one_riset_not_found(self):
        hibahs = self.client.get(reverse('hibah:hibah_list'))
        response2 = self.client.get(
            reverse("hibah:riset_detail", kwargs={'id': hibahs.data['results'][2]['id']})
        )
        self.assertEqual(response2.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_item(self):
        response = self.client.get(reverse('hibah:hibah_list'))
        self.client.delete(
            reverse("hibah:riset_detail", kwargs={'id': response.data['results'][0]['id']})
        )
        self.assertEqual(Hibah.objects.all().count(), 2)


class GetPengmasDetailTest(APITestCase):
    """Test Pengmas Detail API view"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Admin",
            last_name="Pengmas",
            email="admin.pengmas@abc123.com",
            username="admin_pengmas",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        Hibah.objects.create(
            judul_penelitian='Hibah A',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah B',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        Hibah.objects.create(
            judul_penelitian='Hibah C',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )

    def test_retrieve_one_pengmas(self):
        hibahs = self.client.get(reverse('hibah:hibah_list'))
        response = self.client.get(
            reverse('hibah:pengabdian_masyarakat_detail', kwargs={'id': hibahs.data['results'][2]['id']})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['judul_penelitian'], 'Hibah C')

    def test_retrieve_one_pengmas_not_found(self):
        hibahs = self.client.get(reverse('hibah:hibah_list'))
        response2 = self.client.get(
            reverse('hibah:pengabdian_masyarakat_detail', kwargs={'id': hibahs.data['results'][1]['id']})
        )
        self.assertEqual(response2.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_item(self):
        response = self.client.get(reverse('hibah:hibah_list'))
        self.client.delete(
            reverse("hibah:pengabdian_masyarakat_detail", kwargs={'id': response.data['results'][2]['id']})
        )
        self.assertEqual(Hibah.objects.all().count(), 2)


class UpdateHibahTest(APITestCase):
    """ Test module for Update Hibah API """

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Admin",
            last_name="Hibah",
            email="admin.hibah@abc123.com",
            username="admin_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])
        self.hibah = Hibah.objects.create(
            judul_penelitian='Hibah A',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        self.valid_data = {
            "peneliti_utama": {
                "nama_lengkap": "Dosen A"
            },
            "anggota_peneliti": [],
            "lab": None,
            "pemberi_dana": None,
            "topik_penelitian": [
                {
                    "nama_topik": "test"
                }
            ],
            "judul_penelitian": "Hibah A Updated",
            "tahun": "2022",
            "tipe_kegiatan": "Pengabdian Masyarakat",
            "status_usulan": "Lanjutan"
        }
        self.reverse_url = reverse("hibah:hibah_detail", kwargs={'id': self.hibah.id})

    def test_update_hibah(self):
        before_edit = self.client.get(self.reverse_url)
        response = self.client.put(
            self.reverse_url,
            data=self.valid_data,
            content_type='application/json'
        )
        after_edit = self.client.get(self.reverse_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, after_edit.data)
        self.assertNotEqual(response.data, before_edit.data)
