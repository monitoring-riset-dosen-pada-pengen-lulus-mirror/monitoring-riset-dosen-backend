from django.test import TestCase

from kegiatan.models import Hibah, Kegiatan, PemberiDana


class PemberiDanaModelTest(TestCase):
    def setUp(self):
        self.pemberi_dana = PemberiDana.objects.create(
            nama='KEMENRISTEKDIKTI'
        )

    def test_string_representation(self):
        """
        Testing the case whether or not PemberiDana
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.pemberi_dana),
            'KEMENRISTEKDIKTI'
        )


class HibahModelTest(TestCase):
    def setUp(self):
        self.hibah = Hibah.objects.create(
            judul_penelitian='Penelitian Hibah',
            tahun=2022,
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Hibah
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.hibah),
            'Unknown | 2022 | Penelitian Hibah'
        )


class KegiatanModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            tahun=2022,
            keterangan="Testing"
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Hibah
        model's __str__() is working properly
        """
        self.assertEqual(
            str(self.kegiatan),
            'Unknown | 2022'
        )
