from django.utils.datastructures import MultiValueDictKeyError
from pyexcel_xls import get_data as xls_get
from pyexcel_xlsx import get_data as xlsx_get
from rest_framework import status
from rest_framework.generics import (
    CreateAPIView, ListAPIView, RetrieveDestroyAPIView, RetrieveUpdateAPIView,
)
from rest_framework.response import Response

from commons.utils.date import get_current_year
from kegiatan.mixins import HibahMixin, KegiatanMixin
from kegiatan.models import Hibah, Kegiatan, PemberiDana
from kegiatan.serializers import (
    ExcelKegiatanSerializer, HibahSerializer, KegiatanHibahSerializer,
    KegiatanNonHibahSerializer, KegiatanSerializer, OpsiPemberiDanaSerializer,
    PemberiDanaSerializer,
)
from kegiatan.utils import get_kegiatan_serializer

from .swagger_params import pengmas_list_fields, riset_list_fields


class PemberiDanaList(ListAPIView):
    pagination_class = None
    serializer_class = PemberiDanaSerializer
    queryset = PemberiDana.objects.all()


class OpsiPemberiDanaList(ListAPIView):
    serializer_class = OpsiPemberiDanaSerializer
    pagination_class = None
    search_fields = ['nama']
    queryset = PemberiDana.objects.all()


class HibahList(HibahMixin, ListAPIView):
    serializer_class = HibahSerializer
    queryset = Hibah.objects.all()


class HibahDetail(RetrieveUpdateAPIView):
    serializer_class = HibahSerializer
    lookup_field = 'id'
    queryset = Hibah.objects.all()


class PengmasList(KegiatanMixin, ListAPIView):
    serializer_class = KegiatanSerializer
    queryset = Kegiatan.objects.filter(tipe_kegiatan=Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT)

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        return serializer_class(*args, **kwargs, fields=pengmas_list_fields)


class PengmasDetail(RetrieveDestroyAPIView):
    lookup_field = 'id'

    def get_queryset(self):
        uuid = self.kwargs['id']
        return Kegiatan.specify_kegiatan(
            uuid, {'tipe_kegiatan': Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT})

    def get_serializer_class(self):
        uuid = self.kwargs['id']
        return get_kegiatan_serializer(uuid)


class RisetList(KegiatanMixin, ListAPIView):
    serializer_class = KegiatanSerializer
    queryset = Kegiatan.objects.filter(tipe_kegiatan=Kegiatan.TipeKegiatan.RISET)

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        return serializer_class(*args, **kwargs, fields=riset_list_fields)


class RisetDetail(RetrieveDestroyAPIView):
    lookup_field = 'id'

    def get_queryset(self):
        uuid = self.kwargs['id']
        return Kegiatan.specify_kegiatan(
            uuid, {'tipe_kegiatan': Hibah.TipeKegiatan.RISET})

    def get_serializer_class(self):
        uuid = self.kwargs['id']
        return get_kegiatan_serializer(uuid)


class RisetNonHibahCreate(CreateAPIView):
    serializer_class = KegiatanNonHibahSerializer

    def post(self, request, *args, **kwargs):
        request.data['tipe_kegiatan'] = Kegiatan.TipeKegiatan.RISET
        return super(RisetNonHibahCreate, self).post(request, *args, **kwargs)


class RisetHibahCreate(CreateAPIView):
    serializer_class = KegiatanHibahSerializer

    def post(self, request, *args, **kwargs):
        request.data['tipe_kegiatan'] = Kegiatan.TipeKegiatan.RISET
        return super(RisetHibahCreate, self).post(request, *args, **kwargs)


class PengmasNonHibahCreate(CreateAPIView):
    serializer_class = KegiatanNonHibahSerializer

    def post(self, request, *args, **kwargs):
        request.data['tipe_kegiatan'] = Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT
        return super(PengmasNonHibahCreate, self).post(request, *args, **kwargs)


class PengmasHibahCreate(CreateAPIView):
    serializer_class = KegiatanHibahSerializer

    def post(self, request, *args, **kwargs):
        request.data['tipe_kegiatan'] = Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT
        return super(PengmasHibahCreate, self).post(request, *args, **kwargs)


class UploadKegiatan(CreateAPIView):
    def get_item_or_default(self, list_query, index, default):
        return list_query[index] if index < len(list_query) else default

    def post(self, request, format=None):
        try:
            excel_file = request.FILES.get('uploaded_file')
        except MultiValueDictKeyError:
            return Response({'message': 'File failed to upload'}, status=status.HTTP_400_BAD_REQUEST)

        if (str(excel_file).split('.')[-1] == 'xls'):
            data = xls_get(excel_file)
        elif (str(excel_file).split('.')[-1] == 'xlsx'):
            data = xlsx_get(excel_file)
        else:
            return Response({'message': 'File type not supported'}, status=status.HTTP_400_BAD_REQUEST)

        hibah_excel = data['HIBAH']

        if (len(hibah_excel) > 2):
            hibah_record = hibah_excel[2:]
            for hibah in hibah_record:
                if len(hibah) == 0:
                    break
                else:
                    judul_penelitian = self.get_item_or_default(hibah, 1, "")
                    data = {
                        "judul_penelitian": judul_penelitian,
                        "tahun": self.get_item_or_default(hibah, 2, get_current_year()),
                        "tipe_kegiatan": self.get_item_or_default(hibah, 3, "").strip(),
                        "no_kontrak_hibah": self.get_item_or_default(hibah, 4, "").strip(),
                        "peneliti_utama": self.get_item_or_default(hibah, 5, None),
                        "lab": self.get_item_or_default(hibah, 6, None),
                        "anggota_peneliti": self.get_item_or_default(hibah, 7, None),
                        "no_nota_dinas": self.get_item_or_default(hibah, 8, "").strip(),
                        "skema_pembiayaan": self.get_item_or_default(hibah, 11, "").strip(),
                        "nilai_hibah_nasional": self.get_item_or_default(hibah, 13, ""),
                        "nilai_hibah_internasional": self.get_item_or_default(hibah, 14, ""),
                        "pemberi_dana": self.get_item_or_default(hibah, 15, None),
                        "status_usulan": self.get_item_or_default(hibah, 17, "").strip(),
                    }

                    query_existing_kegiatan = Kegiatan.objects.filter(judul_penelitian__iexact=judul_penelitian)
                    if query_existing_kegiatan.exists():
                        instance = query_existing_kegiatan[0]
                        serializer = ExcelKegiatanSerializer(instance=instance, data=data)
                    else:
                        serializer = ExcelKegiatanSerializer(data=data)

                    if (serializer.is_valid()):
                        serializer.save()
                    else:
                        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response({"message": "sukses"}, status=status.HTTP_200_OK)
