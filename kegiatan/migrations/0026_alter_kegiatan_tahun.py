# Generated by Django 4.0.2 on 2022-05-17 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0025_alter_hibah_nilai_hibah_internasional_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kegiatan',
            name='tahun',
            field=models.IntegerField(choices=[(2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023)], default=2022),
        ),
    ]
