# Generated by Django 4.0.2 on 2022-05-10 09:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0012_remove_kegiatan_funding_scheme_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='kegiatan',
            options={'ordering': ['-tahun']},
        ),
        migrations.RenameField(
            model_name='hibah',
            old_name='grant_title',
            new_name='judul_hibah',
        ),
        migrations.RenameField(
            model_name='hibah',
            old_name='funding_scheme',
            new_name='skema_pembiayaan',
        ),
        migrations.RenameField(
            model_name='kegiatan',
            old_name='principal_investigator',
            new_name='peneliti_utama',
        ),
        migrations.RenameField(
            model_name='kegiatan',
            old_name='year',
            new_name='tahun',
        ),
    ]
