# Generated by Django 4.0.2 on 2022-03-19 18:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hibah',
            name='status_pelaksanaan',
            field=models.CharField(blank=True, choices=[('Sedang Berlangsung', 'Sedang Berlangsung'), ('Sudah Selesai', 'Sudah Selesai'), ('Terlambat', 'Terlambat')], default='Sedang Berlangsung', max_length=255),
        ),
        migrations.AlterField(
            model_name='hibah',
            name='status_usulan',
            field=models.CharField(blank=True, choices=[('Belum', 'Belum'), ('Lanjutan', 'Lanjutan')], default='', max_length=255),
        ),
    ]
