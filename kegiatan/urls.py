from django.urls import path
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status

from kegiatan.serializers import KegiatanSerializer

from . import views
from .swagger_params import (
    pengmas_list_fields, riset_list_fields, status_usulan_params,
    tipe_pendanaan_params, year_params,
)

decorated_PengmasList_view = swagger_auto_schema(
    method='get',
    responses={status.HTTP_200_OK: KegiatanSerializer(fields=pengmas_list_fields)},
    manual_parameters=[
        status_usulan_params,
        year_params,
        tipe_pendanaan_params,
    ],
)(views.PengmasList.as_view())

decorated_RisetList_view = swagger_auto_schema(
    method='get',
    responses={status.HTTP_200_OK: KegiatanSerializer(fields=riset_list_fields)},
    manual_parameters=[
        status_usulan_params,
        year_params,
        tipe_pendanaan_params,
    ],
)(views.RisetList.as_view())

app_name = 'hibah'
urlpatterns = [
    path('pemberi-dana/', views.PemberiDanaList.as_view(), name='pemberi_dana_list'),
    path('pemberi-dana/opsi-pemberi-dana/', views.OpsiPemberiDanaList.as_view(), name='opsi_pemberi_dana_list'),
    path('hibah/', views.HibahList.as_view(), name='hibah_list'),
    path('hibah/<uuid:id>', views.HibahDetail.as_view(), name='hibah_detail'),
    path('riset/', decorated_RisetList_view, name='riset_list'),
    path('riset/tambah-hibah/', views.RisetHibahCreate.as_view(), name='riset_hibah_create'),
    path('riset/tambah-non-hibah/', views.RisetNonHibahCreate.as_view(), name='riset_non_hibah_create'),
    path('riset/<uuid:id>', views.RisetDetail.as_view(), name='riset_detail'),
    path('pengabdian-masyarakat/', decorated_PengmasList_view, name='pengabdian_masyarakat_list'),
    path('pengabdian-masyarakat/<uuid:id>', views.PengmasDetail.as_view(), name='pengabdian_masyarakat_detail'),
    path(
        'pengabdian-masyarakat/tambah-hibah/',
        views.PengmasHibahCreate.as_view(),
        name='pengabdian_masyarakat_hibah_create'
    ),
    path(
        'pengabdian-masyarakat/tambah-non-hibah/',
        views.PengmasNonHibahCreate.as_view(),
        name='pengabdian_masyarakat_non_hibah_create'
    ),
    path('upload-data', views.UploadKegiatan.as_view(), name='kegiatan_upload_excel'),
]
