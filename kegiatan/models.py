import uuid

from django.db import models

from author.models import Pembuat
from commons.utils.authors import add_et_al_to_authors
from commons.utils.date import get_current_year, year_choices
from faculty.models import Lab, TopikPenelitian


class PemberiDana(models.Model):
    class TipePendanaan(models.TextChoices):
        INTERNAL = "Internal"
        EKSTERNAL = "Eksternal"

    nama = models.CharField(max_length=255)
    tipe_pendanaan = models.CharField(max_length=255, choices=TipePendanaan.choices, default=TipePendanaan.INTERNAL)

    def __str__(self) -> str:
        return self.nama


class Kegiatan(models.Model):
    class TipeKegiatan(models.TextChoices):
        RISET = "Riset"
        PENGABDIAN_MASYARAKAT = "Pengabdian Masyarakat"

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    tahun = models.IntegerField(choices=year_choices(), default=get_current_year())
    judul_penelitian = models.CharField(max_length=255)
    peneliti_utama = models.ForeignKey(
        Pembuat,
        related_name='hibah_investigator',
        on_delete=models.SET_NULL,
        null=True
    )
    lab = models.ForeignKey(
        Lab,
        related_name='hibah',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    anggota_peneliti = models.ManyToManyField(
        Pembuat,
        related_name='hibah_peneliti',
        blank=True
    )
    tipe_kegiatan = models.CharField(
        max_length=255,
        blank=True,
        choices=TipeKegiatan.choices,
        default=TipeKegiatan.RISET
    )
    topik_penelitian = models.ManyToManyField(
        TopikPenelitian,
        related_name='topik_penelitian',
        blank=True
    )
    keterangan = models.TextField(blank=True)
    is_non_hibah = models.BooleanField(default=False)

    class Meta:
        ordering = ['-tahun']

    @staticmethod
    def get_tahun_attr():
        return 'tahun'

    @staticmethod
    def specify_kegiatan(uuid: str, filters={}):
        if (Hibah.objects.filter(id=uuid).exists()):
            return Hibah.objects.filter(id=uuid, **filters)
        return Kegiatan.objects.filter(id=uuid, **filters)

    def __str__(self) -> str:
        author_with_et_al = add_et_al_to_authors(
            self.anggota_peneliti,
            self.peneliti_utama
        )
        return f'{author_with_et_al} | {self.tahun}'


class Hibah(Kegiatan):
    class StatusUsulan(models.TextChoices):
        BARU = "Baru"
        LANJUTAN = "Lanjutan"

    nilai_hibah_internasional = models.IntegerField(null=True, blank=True, default=0)
    nilai_hibah_nasional = models.IntegerField(null=True, blank=True, default=0)
    skema_pembiayaan = models.CharField(max_length=255, blank=True)
    no_kontrak_hibah = models.CharField(max_length=255, blank=True)
    pemberi_dana = models.ForeignKey(
        PemberiDana,
        related_name='hibah',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    status_usulan = models.CharField(
        max_length=255,
        blank=True,
        choices=StatusUsulan.choices,
        default=''
    )
    no_nota_dinas = models.CharField(max_length=255, blank=True)

    def __str__(self) -> str:
        author_with_et_al = add_et_al_to_authors(
            self.anggota_peneliti,
            self.peneliti_utama
        )
        return f'{author_with_et_al} | {self.tahun} | {self.judul_penelitian}'

    @staticmethod
    def calculate_pendanaan_from_ten_years_ago(queryset):
        result = {}
        current_year = get_current_year()
        lower_bound_year = current_year - 10
        for year in range(current_year, lower_bound_year, -1):
            queryset_filtered_year = queryset.filter(tahun=year)
            total_pendanaan = 0
            for query_object in queryset_filtered_year:
                if query_object.nilai_hibah_nasional is not None:
                    total_pendanaan += query_object.nilai_hibah_nasional
                else:
                    total_pendanaan += 0

                if query_object.nilai_hibah_internasional is not None:
                    total_pendanaan += query_object.nilai_hibah_internasional
                else:
                    total_pendanaan += 0

            result[year] = total_pendanaan
        return result

    @staticmethod
    def get_pendanaan_hibah_according_tipe_kegiatan(tipe_kegiatan: str) -> dict:
        hibahs = Hibah.objects.filter(tipe_kegiatan=tipe_kegiatan)
        return Hibah.calculate_pendanaan_from_ten_years_ago(hibahs)

    @staticmethod
    def get_pendanaan_hibah_according_tipe_pendanaan(
            tipe_pendanaan: str, is_national=True) -> dict:
        result = {}
        current_year = get_current_year()
        lower_bound_year = current_year - 10
        queryset = Hibah.objects.all()
        queryset = queryset.filter(pemberi_dana__tipe_pendanaan=tipe_pendanaan)
        for year in range(current_year, lower_bound_year, -1):
            total_pendanaan = 0
            queryset_filtered_year = queryset.filter(tahun=year)
            for query_object in queryset_filtered_year:
                if is_national:
                    if query_object.nilai_hibah_nasional is not None:
                        total_pendanaan += \
                            query_object.nilai_hibah_nasional
                    else:
                        total_pendanaan += 0
                else:
                    if query_object.nilai_hibah_internasional is not None:
                        total_pendanaan += \
                            query_object.nilai_hibah_internasional
                    else:
                        total_pendanaan += 0
            result[year] = total_pendanaan
        return result
