from commons.mixins import FilterKegiatanParamMixin
from kegiatan.filters import HibahFilterSet, KegiatanFilterSet


class KegiatanMixin(FilterKegiatanParamMixin):
    search_fields = ['judul_penelitian']
    filter_class = KegiatanFilterSet
    ordering_fields = ['tahun']


class HibahMixin(FilterKegiatanParamMixin):
    search_fields = ['judul_penelitian']
    filter_class = HibahFilterSet
    ordering_fields = ['tahun']
