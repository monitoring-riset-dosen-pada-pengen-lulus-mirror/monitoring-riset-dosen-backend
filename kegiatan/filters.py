from django_filters import ChoiceFilter, FilterSet

from kegiatan.models import Hibah, Kegiatan, PemberiDana


class KegiatanFilterSet(FilterSet):

    class Meta:
        model = Kegiatan
        fields = ['lab', 'is_non_hibah']


class HibahFilterSet(FilterSet):
    tipe_pendanaan = ChoiceFilter(field_name='pemberi_dana__tipe_pendanaan', choices=PemberiDana.TipePendanaan.choices)

    class Meta:
        model = Hibah
        fields = ['tipe_pendanaan', 'lab', 'status_usulan', 'tipe_kegiatan']
