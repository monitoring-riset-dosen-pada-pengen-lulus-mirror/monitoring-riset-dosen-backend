from kegiatan.models import Hibah
from kegiatan.serializers import HibahSerializer, KegiatanSerializer


def get_kegiatan_serializer(uuid: str):
    if (Hibah.objects.filter(id=uuid).exists()):
        return HibahSerializer
    return KegiatanSerializer
