from django.contrib import admin

from kegiatan.models import Hibah, Kegiatan, PemberiDana

admin.site.register([
    Kegiatan,
    Hibah,
    PemberiDana
])
