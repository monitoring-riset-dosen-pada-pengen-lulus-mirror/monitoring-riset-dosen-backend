FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

WORKDIR /root

COPY requirements.txt /root
RUN pip3 install -r requirements.txt

HEALTHCHECK  --interval=10s --timeout=5s --retries=5 \
  CMD curl --fail http://localhost:8000/admin/ || exit 1

COPY . /root
RUN python manage.py collectstatic --noinput
CMD python3 manage.py migrate && \
    gunicorn sihibah.wsgi:application --bind 0.0.0.0:$PORT
