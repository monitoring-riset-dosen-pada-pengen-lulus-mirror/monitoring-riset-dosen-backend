from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from rest_framework.test import APITestCase

from account.utils import get_tokens_for_user
from commons.utils.date import get_current_year
from kegiatan.models import Hibah, Kegiatan, PemberiDana
from produk.models import HKI, Paten
from produk.tests.test_views import create_bearer


class DashboardTest(APITestCase):
    """Test module for dashboard API"""

    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION=create_bearer(self.token[0]))
        self.pemberi_dana_internal = PemberiDana.objects.create(
            tipe_pendanaan=PemberiDana.TipePendanaan.INTERNAL
        )
        self.pemberi_dana_eksternal = PemberiDana.objects.create(
            tipe_pendanaan=PemberiDana.TipePendanaan.EKSTERNAL
        )
        self.api_pendanaan_hibah = 'pendanaan_hibah'
        self.api_pendanaan_hibah_total = 'total'
        self.judul1 = "Hibah 1"
        self.nilai_hibah1 = 1000
        self.nilai_hibah2 = 2000
        self.year1 = get_current_year()
        self.url_dashboard = "dashboard:dashboard"

    def test_two_hibah_in_current_year_return_grant_value_sum(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            nilai_hibah_nasional=self.nilai_hibah1,
            pemberi_dana=self.pemberi_dana_internal
        )
        Hibah.objects.create(
            judul_penelitian='Hibah 2',
            tahun=self.year1,
            nilai_hibah_nasional=self.nilai_hibah2,
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah][self.api_pendanaan_hibah_total][self.year1]
        self.assertEqual(result, self.nilai_hibah1 + self.nilai_hibah2)

    def test_one_hibah_in_ten_years_past_return_grant_value(self):
        year = self.year1 - 9
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=year,
            nilai_hibah_nasional=self.nilai_hibah1,
            pemberi_dana=self.pemberi_dana_internal
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah][self.api_pendanaan_hibah_total][year]
        self.assertEqual(result, self.nilai_hibah1)

    def test_one_hibah_in_eleven_years_past_return_error(self):
        year = self.year1 - 10
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=year,
            nilai_hibah_nasional=self.nilai_hibah1,
            pemberi_dana=self.pemberi_dana_internal
        )
        response = self.client.get(reverse(self.url_dashboard))
        try:
            response.data[self.api_pendanaan_hibah][self.api_pendanaan_hibah_total][year]
        except KeyError as res:
            self.assertEqual(str(res), str(year))

    def test_one_hibah_with_grant_value_none_return_zero(self):
        year = self.year1
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=year,
            pemberi_dana=self.pemberi_dana_internal
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah][self.api_pendanaan_hibah_total][self.year1]
        self.assertEqual(result, 0)

    def test_hibah_internal_ui_return_grant_value(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_internal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.RISET,
            nilai_hibah_nasional=self.nilai_hibah1
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah]['internal_ui'][self.year1]
        self.assertEqual(result, self.nilai_hibah1)

    def test_hibah_eksternal_nasional_return_grant_value(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.RISET,
            nilai_hibah_nasional=self.nilai_hibah1
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah]['eksternal_nasional'][self.year1]
        self.assertEqual(result, self.nilai_hibah1)

    def test_hibah_eksternal_internasional_return_grant_value(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.RISET,
            nilai_hibah_internasional=self.nilai_hibah1
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data[self.api_pendanaan_hibah]['eksternal_internasional'][self.year1]
        self.assertEqual(result, self.nilai_hibah1)

    def test_hibah_riset_return_grant_value(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.RISET,
            nilai_hibah_internasional=self.nilai_hibah1
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data['pendanaan_riset'][self.year1]
        self.assertEqual(result, self.nilai_hibah1)

    def test_hibah_pengmas_return_grant_value(self):
        Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT,
            nilai_hibah_internasional=self.nilai_hibah1
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data['pendanaan_pengmas'][self.year1]
        self.assertEqual(result, self.nilai_hibah1)

    def test_hki_return_each_1(self):
        hibah1 = Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT,
            nilai_hibah_internasional=self.nilai_hibah1
        )
        no_reg = "1"
        HKI.objects.create(
            jenis_hki=HKI.JenisHKI.PATEN,
            tahun=self.year1,
            hibah=hibah1,
            no_registrasi_hak_cipta=no_reg
        )
        no_reg = no_reg + "1"
        HKI.objects.create(
            jenis_hki=HKI.JenisHKI.BUKU,
            tahun=self.year1,
            hibah=hibah1,
            no_registrasi_hak_cipta=no_reg
        )
        no_reg = no_reg + "1"
        HKI.objects.create(
            jenis_hki=HKI.JenisHKI.KARYA_TULIS,
            tahun=self.year1,
            hibah=hibah1, no_registrasi_hak_cipta=no_reg
        )
        no_reg = no_reg + "1"
        HKI.objects.create(
            jenis_hki=HKI.JenisHKI.PROGRAM_KOMPUTER,
            tahun=self.year1,
            hibah=hibah1, no_registrasi_hak_cipta=no_reg
        )
        no_reg = no_reg + "1"
        HKI.objects.create(
            jenis_hki=HKI.JenisHKI.BASIS_DATA,
            tahun=self.year1,
            hibah=hibah1, no_registrasi_hak_cipta=no_reg
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data['jumlah_hki']
        for k, v in result.items():
            self.assertEqual(v[self.year1], 1)

    def test_paten_return_each_1(self):
        hibah1 = Hibah.objects.create(
            judul_penelitian=self.judul1,
            tahun=self.year1,
            pemberi_dana=self.pemberi_dana_eksternal,
            tipe_kegiatan=Kegiatan.TipeKegiatan.PENGABDIAN_MASYARAKAT,
            nilai_hibah_internasional=self.nilai_hibah1
        )
        no_reg = "1"
        no_agenda = "1"
        Paten.objects.create(
            jenis_paten=Paten.JenisPaten.PATEN,
            tahun=self.year1, no_registrasi_paten=no_reg, hibah=hibah1,
            nomor_agenda=no_agenda
        )
        no_reg = no_reg + "1"
        Paten.objects.create(
            jenis_paten=Paten.JenisPaten.PATEN_SEDERHANA,
            tahun=self.year1, no_registrasi_paten=no_reg, hibah=hibah1,
            nomor_agenda=no_agenda+"1"
        )
        response = self.client.get(reverse(self.url_dashboard))
        result = response.data['jumlah_paten']
        for k, v in result.items():
            self.assertEqual(v[self.year1], 1)
