from kegiatan.models import Hibah, PemberiDana
from produk.models import HKI, Paten


class DashboardSerializer():

    def __init__(self) -> None:
        result = {}
        result['pendanaan_hibah'] = {}
        result['pendanaan_hibah']['total'] = \
            Hibah.calculate_pendanaan_from_ten_years_ago(
                Hibah.objects.all())
        result['pendanaan_hibah']['internal_ui'] = \
            Hibah.get_pendanaan_hibah_according_tipe_pendanaan(
                PemberiDana.TipePendanaan.INTERNAL
            )
        result['pendanaan_hibah']['eksternal_nasional'] = \
            Hibah.get_pendanaan_hibah_according_tipe_pendanaan(
                PemberiDana.TipePendanaan.EKSTERNAL
            )
        result['pendanaan_hibah']['eksternal_internasional'] = \
            Hibah.get_pendanaan_hibah_according_tipe_pendanaan(
                PemberiDana.TipePendanaan.EKSTERNAL, is_national=False
            )
        result['pendanaan_riset'] = \
            Hibah.get_pendanaan_hibah_according_tipe_kegiatan(
                Hibah.TipeKegiatan.RISET)
        result['pendanaan_pengmas'] = \
            Hibah.get_pendanaan_hibah_according_tipe_kegiatan(
                Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT)
        result['jumlah_hki'] = \
            HKI.get_jumlah_hki()
        result['jumlah_paten'] = \
            Paten.get_jumlah_paten()
        self.data = result
