from django.urls import path

from . import views

app_name = 'dashboard'
urlpatterns = [
    path('', views.DashboardDetail.as_view(), name='dashboard'),
]
