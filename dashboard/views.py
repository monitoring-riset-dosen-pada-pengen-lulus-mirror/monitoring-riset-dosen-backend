from rest_framework.response import Response
from rest_framework.views import APIView

from dashboard.serializers import DashboardSerializer


class DashboardDetail(APIView):

    def get(self, request):
        serializer = DashboardSerializer()
        return Response(serializer.data)
