from django.utils import timezone
from rest_framework import serializers

from commons.serializers import OpsiSerializer
from kegiatan.models import Hibah

from .models import Pembuat


class PembuatSerializer(serializers.ModelSerializer):
    jumlah_hibah = serializers.SerializerMethodField('get_jumlah_hibah')
    jumlah_hki = serializers.SerializerMethodField('get_jumlah_hki')
    jumlah_paten = serializers.SerializerMethodField('get_jumlah_paten')
    jumlah_riset = serializers.SerializerMethodField('get_jumlah_riset')
    jumlah_pengmas = serializers.SerializerMethodField('get_jumlah_pengmas')

    class Meta:
        model = Pembuat
        fields = '__all__'

    def get_jumlah_hibah(self, obj):
        amount_become_investigator = obj.hibah_investigator.count()
        amount_become_peneliti = obj.hibah_peneliti.count()
        return amount_become_peneliti + amount_become_investigator

    def get_jumlah_hki(self, obj):
        return obj.hki_pencipta.count()

    def get_jumlah_paten(self, obj):
        return obj.paten_pencipta.count()

    def get_jumlah_riset(self, obj):
        amount_become_investigator = obj.hibah_investigator.filter(
            tipe_kegiatan=Hibah.TipeKegiatan.RISET).count()
        amount_become_peneliti = obj.hibah_peneliti.filter(
            tipe_kegiatan=Hibah.TipeKegiatan.RISET).count()
        return amount_become_investigator + amount_become_peneliti

    def get_jumlah_pengmas(self, obj):
        amount_become_investigator = obj.hibah_investigator.filter(
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT).count()
        amount_become_peneliti = obj.hibah_peneliti.filter(
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT).count()
        return amount_become_investigator + amount_become_peneliti


class OpsiPembuatSerializer(OpsiSerializer):
    def get_label(self, obj):
        return obj.nama_lengkap

    def get_value(self, obj):
        return obj.uuid

    class Meta:
        model = Pembuat
        fields = ('label', 'value',)


class DetailPembuatSerializer(PembuatSerializer):
    jumlah_pendanaan_nasional = serializers.SerializerMethodField('get_jumlah_pendanaan_nasional')
    jumlah_pendanaan_internasional = serializers.SerializerMethodField('get_jumlah_pendanaan_internasional')

    def get_jumlah_pendanaan_nasional(self, obj):
        current_year = timezone.now().year
        filters = {"tahun": current_year}
        amount_dana_hibah_nasional = 0
        list_kegiatan = obj.hibah_peneliti.filter(**filters) | obj.hibah_investigator.filter(**filters)
        for kegiatan in list_kegiatan:
            try:
                hibah = Hibah.objects.get(id=kegiatan.id)
                nilai_hibah = hibah.nilai_hibah_nasional if hibah.nilai_hibah_nasional else 0
                amount_dana_hibah_nasional = amount_dana_hibah_nasional + nilai_hibah
            finally:
                continue
        return amount_dana_hibah_nasional

    def get_jumlah_pendanaan_internasional(self, obj):
        current_year = timezone.now().year
        filters = {"tahun": current_year}
        amount_dana_hibah_internasional = 0
        list_kegiatan = obj.hibah_peneliti.filter(**filters) | obj.hibah_investigator.filter(**filters)
        for kegiatan in list_kegiatan:
            try:
                hibah = Hibah.objects.get(id=kegiatan.id)
                nilai_hibah = hibah.nilai_hibah_internasional if hibah.nilai_hibah_internasional else 0
                amount_dana_hibah_internasional = amount_dana_hibah_internasional + nilai_hibah
            finally:
                continue
        return amount_dana_hibah_internasional
