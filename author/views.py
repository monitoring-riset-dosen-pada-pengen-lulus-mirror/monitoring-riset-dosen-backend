from django.db.models import Q
from rest_framework.generics import ListAPIView, RetrieveAPIView

from author.models import Pembuat
from author.serializers import (
    DetailPembuatSerializer, OpsiPembuatSerializer, PembuatSerializer,
)
from kegiatan.models import Hibah
from kegiatan.serializers import KegiatanSerializer
from kegiatan.swagger_params import pengmas_list_fields, riset_list_fields
from produk.models import HKI, Paten
from produk.serializers import HKISerializer, PatenSerializer


class PembuatList(ListAPIView):
    serializer_class = PembuatSerializer
    search_fields = ['nama_lengkap']
    queryset = Pembuat.objects.all()

    def get_queryset(self):
        return self.queryset


class OpsiPembuatList(ListAPIView):
    serializer_class = OpsiPembuatSerializer
    pagination_class = None
    search_fields = ['nama_lengkap']
    queryset = Pembuat.objects.all()


class PembuatDetail(RetrieveAPIView):
    serializer_class = DetailPembuatSerializer
    lookup_field = 'uuid'
    queryset = Pembuat.objects.all()


class PembuatRisetList(ListAPIView):
    serializer_class = KegiatanSerializer
    queryset = Hibah.objects.filter(tipe_kegiatan=Hibah.TipeKegiatan.RISET)

    def get_queryset(self):
        id = self.kwargs['uuid']
        return self.queryset.filter(Q(anggota_peneliti__uuid=id) | Q(peneliti_utama__uuid=id))

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        return serializer_class(*args, **kwargs, fields=riset_list_fields)


class PembuatPengmasList(ListAPIView):
    serializer_class = KegiatanSerializer
    queryset = Hibah.objects.filter(tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT)

    def get_queryset(self):
        id = self.kwargs['uuid']
        return self.queryset.filter(Q(anggota_peneliti__uuid=id) | Q(peneliti_utama__uuid=id))

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        return serializer_class(*args, **kwargs, fields=pengmas_list_fields)


class PembuatHKIList(ListAPIView):
    serializer_class = HKISerializer
    queryset = HKI.objects.all()

    def get_queryset(self):
        id = self.kwargs['uuid']
        return self.queryset.filter(pencipta__uuid=id)


class PembuatPatenList(ListAPIView):
    serializer_class = PatenSerializer
    queryset = Paten.objects.all()

    def get_queryset(self):
        id = self.kwargs['uuid']
        return self.queryset.filter(pencipta__uuid=id)
