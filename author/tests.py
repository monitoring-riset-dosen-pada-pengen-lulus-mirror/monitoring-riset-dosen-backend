from django.test import Client, TestCase
from django.urls import reverse

from account.utils import get_tokens_for_user
from author.models import Pembuat
from author.serializers import PembuatSerializer
from faculty.admin import User
from kegiatan.models import Hibah
from produk.models import HKI, Paten


class PembuatTest(TestCase):
    def setUp(self):
        self.pembuat = Pembuat.objects.create(
            nama_lengkap='Testing nama',
        )
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])

    def test_string_representation(self):
        expected = self.pembuat.nama_lengkap
        result = str(self.pembuat)
        self.assertEqual(
            expected,
            result
        )

    def test_create_object(self):
        amount_obj_before = Pembuat.objects.count()
        Pembuat.objects.create(
            nama_lengkap='Testing nama 1',
        )
        expected = amount_obj_before + 1
        result = Pembuat.objects.count()
        self.assertEqual(expected,
                         result)

    def test_update_object(self):
        new_name = "Testing nama 3"
        self.pembuat.nama_lengkap = new_name
        self.pembuat.save()
        expected = new_name
        result = Pembuat.objects.get(
            nama_lengkap=new_name).nama_lengkap
        self.assertEqual(expected,
                         result)

    def test_delete_object(self):
        amount_obj_before = Pembuat.objects.count()
        self.pembuat.delete()
        expected = amount_obj_before-1
        result = Pembuat.objects.count()
        self.assertEqual(expected, result)

    def test_api_return_200(self):
        response = self.client.get(reverse('pembuat:pembuat_list'))
        self.assertEqual(response.status_code, 200)

    def test_get_list_pembuat_return_all_objects(self):
        response = self.client.get(reverse('pembuat:pembuat_list'))
        expected = Pembuat.objects.count()
        result = response.data['count']
        self.assertEquals(expected, result)

    def test_get_list_pembuat_pagination_second_page_with_one_object_return_one_object(self):
        Pembuat.objects.create(
            nama_lengkap='Testing nama 1',
        )
        second_page = 2
        amount_objects = 1
        response = self.client.get(
            f"{reverse('pembuat:pembuat_list')}?page={second_page}&page_size={amount_objects}")
        expected = amount_objects
        result = len(response.data['results'])
        self.assertEqual(expected, result)

    def test_get_list_pembuat_next_page(self):
        Pembuat.objects.create(
            nama_lengkap='Testing nama 1',
        )
        first_page = 1
        amount_objects = 1
        response = self.client.get(
            f"{reverse('pembuat:pembuat_list')}?page={first_page}&page_size={amount_objects}")
        expected = first_page + 1
        result = response.data['next']
        self.assertEqual(expected, result)

    def test_get_list_pembuat_previous_page(self):
        Pembuat.objects.create(
            nama_lengkap='Testing nama 1',
        )
        nth_page = 2
        amount_objects = 1
        response = self.client.get(
            f"{reverse('pembuat:pembuat_list')}?page={nth_page}&page_size={amount_objects}")
        expected = nth_page - 1
        result = response.data['previous']
        self.assertEqual(expected, result)

    def test_search_query_pembuat(self):
        response = self.client.get(
            '%s?search=%s' % (
                reverse('pembuat:pembuat_list'),
                'Testing nama',
            ),
        )
        pembuat = Pembuat.objects.filter(nama_lengkap='Testing nama')
        serializer = PembuatSerializer(pembuat, many=True)
        self.assertEqual(response.data['results'], serializer.data)


class DetailPembuatTest(TestCase):
    def setUp(self):
        self.pembuat = Pembuat.objects.create(
            nama_lengkap='Testing nama',
        )
        self.user = User.objects.create_superuser(
            first_name="Asdos Hibah",
            last_name="B",
            email="asdos_hibah@gmail.com",
            username="asdos_hibah",
            password="pasuworudo"
        )
        self.riset = Hibah.objects.create(
            judul_penelitian='Riset 1',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.RISET
        )
        self.pengmas = Hibah.objects.create(
            judul_penelitian='Pengmas 1',
            tahun=2022,
            tipe_kegiatan=Hibah.TipeKegiatan.PENGABDIAN_MASYARAKAT
        )
        self.hki = HKI.objects.create(
            judul_ciptaan='HKI 1',
            hibah=self.riset,
            tahun=2022
        )
        self.paten = Paten.objects.create(
            judul_ciptaan="Paten 1",
            no_registrasi_paten='1',
            nomor_agenda='1',
            hibah=self.riset,
            tahun=2022
        )
        self.token = get_tokens_for_user(self.user)
        self.client = Client(HTTP_AUTHORIZATION='Bearer ' + self.token[0])

    def test_get_detail_pembuat(self):
        response = self.client.get(reverse('pembuat:pembuat_detail', kwargs={'uuid': self.pembuat.uuid}))
        expected = self.pembuat.nama_lengkap
        result = response.data['nama_lengkap']
        self.assertEquals(expected, result)

    def test_get_list_riset_pembuat(self):
        response = self.client.get(reverse('pembuat:pembuat_riset', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(0, result)

        self.riset.anggota_peneliti.add(self.pembuat)

        response = self.client.get(reverse('pembuat:pembuat_riset', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(1, result)

    def test_get_list_pengmas_pembuat(self):
        response = self.client.get(reverse('pembuat:pembuat_pengmas', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(0, result)

        self.pengmas.anggota_peneliti.add(self.pembuat)

        response = self.client.get(reverse('pembuat:pembuat_pengmas', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(1, result)

    def test_get_list_hki_pembuat(self):
        response = self.client.get(reverse('pembuat:pembuat_hki', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(0, result)

        self.hki.pencipta.add(self.pembuat)

        response = self.client.get(reverse('pembuat:pembuat_hki', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(1, result)

    def test_get_list_paten_pembuat(self):
        response = self.client.get(reverse('pembuat:pembuat_paten', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(0, result)

        self.paten.pencipta.add(self.pembuat)

        response = self.client.get(reverse('pembuat:pembuat_paten', kwargs={'uuid': self.pembuat.uuid}))
        result = len(response.data['results'])
        self.assertEquals(1, result)
