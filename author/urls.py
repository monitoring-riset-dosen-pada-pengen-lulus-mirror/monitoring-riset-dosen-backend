from django.urls import path

from . import views

app_name = 'pembuat'
urlpatterns = [
    path('', views.PembuatList.as_view(), name='pembuat_list'),
    path('opsi-pembuat/', views.OpsiPembuatList.as_view(), name='opsi_pembuat_list'),
    path('<str:uuid>/', views.PembuatDetail.as_view(), name='pembuat_detail'),
    path('<str:uuid>/riset/', views.PembuatRisetList.as_view(), name='pembuat_riset'),
    path('<str:uuid>/pengabdian-masyarakat/', views.PembuatPengmasList.as_view(), name='pembuat_pengmas'),
    path('<str:uuid>/hki/', views.PembuatHKIList.as_view(), name='pembuat_hki'),
    path('<str:uuid>/paten/', views.PembuatPatenList.as_view(), name='pembuat_paten'),
]
