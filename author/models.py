import uuid

from django.db import models


class Pembuat(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['uuid', 'nama_lengkap'],
                name='unique_pembuat')
        ]
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nama_lengkap = models.CharField(max_length=255, blank=True)

    def __str__(self) -> str:
        return self.nama_lengkap
