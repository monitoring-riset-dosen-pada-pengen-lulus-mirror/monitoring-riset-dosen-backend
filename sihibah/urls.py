"""sihibah URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView,
)

from account.admin import sso_admin_site

schema_view = get_schema_view(
    openapi.Info(
        title='Sihibah API Docs',
        default_version='v1',
        description='API Documentation for Sistem Informasi Hibah dan Pengabdian Masyarakat',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sso-admin/', sso_admin_site.urls),
    path('api/commons/', include('commons.urls')),
    path('api/dashboard/', include('dashboard.urls')),
    path('api/kegiatan/', include('kegiatan.urls')),
    path('api/faculty/', include('faculty.urls')),
    path('api/account/', include('account.urls')),
    path('api/info-hibah/', include('infohibah.urls')),
    path('api/output-hibah/', include('produk.urls')),
    path('api/pembuat/', include('author.urls')),
    path('api/', include('rest_framework.urls', namespace='api')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
