"""
Django settings for sihibah project.

Generated by 'django-admin startproject' using Django 3.2.8.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""
import os
from datetime import timedelta
from pathlib import Path

from google.oauth2 import service_account

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY',
                            default='django-insecure-ts_adbc9f1i(zc=xiu-@2e2d=hm-ax($_fl^@=0&7_-ndgi!yk')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = int(os.environ.get('DEBUG', default=0))

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cas_ng',
    'django_filters',
    'corsheaders',
    'rest_framework',
    'rest_framework_simplejwt',
    'drf_yasg',
    'storages',
    'infohibah',
    'account',
    'faculty',
    'kegiatan',
    'produk',
    'author',
    'commons',
    'dashboard'
]

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    # Unrestricted access is fine for local development,
    # but in a production environment you may need to
    # restrict access to certain endpoints. Make sure to update this.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': [
        'rest_framework.filters.SearchFilter',
        'rest_framework.filters.OrderingFilter',
        'django_filters.rest_framework.DjangoFilterBackend',
    ],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_PAGINATION_CLASS': 'commons.paginations.StandardResultsSetPagination',
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'sihibah.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'sihibah.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('PG_NAME', default='postgres'),
        'USER': os.environ.get('PG_USER', default='postgres'),
        'HOST': os.environ.get('PG_HOST', default='postgres'),
        'PORT': int(os.environ.get('PG_PORT', default='5432')),
        'PASSWORD': os.environ.get('PG_PASS', default='Admin123*'),
        'TEST': {
            'NAME': 'mytestdatabase',
        },
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Jakarta'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


# Authentication configurations Consists of django-cas-ng,
# djangorestframework-simplejwt, and django-cors-headers configs
# https://djangocas.dev/blog/django-cas-ng-example-project/

# django-cas-ng configurations

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'account.cas.backends.SihibahCASBackend',
)

CAS_LOGIN_URL_NAME = 'account:cas_ng_login'
CAS_SERVER_URL = 'https://sso.ui.ac.id/cas2/'
CAS_VERSION = 2
CAS_STORE_NEXT = True
CAS_CREATE_USER = True
CAS_APPLY_ATTRIBUTES_TO_USER = True
CAS_FORCE_CHANGE_USERNAME_CASE = 'lower'
CAS_LOGOUT_COMPLETELY = True

# django-cors-headers configurations

CORS_ALLOWED_ORIGINS = [
    'http://localhost:3000',
    'https://sihibah-fe.netlify.app'
]
CORS_ALLOWED_ORIGIN_REGEXES = [
    r'^http://localhost:[0-9]+$',
    r'^https://.*\-sihibah-fe\.netlify\.app$',
]
CSRF_TRUSTED_ORIGINS = ['https://*.herokuapp.com',
                        "https://*.127.0.0.1",
                        "http://*.127.0.0.1",
                        'http://*.herokuapp.com', ]
CORS_ALLOW_CREDENTIALS = True

# rest-framework_simplejwt configuration

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(hours=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
}


# Whitenoise configuration

STATIC_ROOT = BASE_DIR / 'staticfiles'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedStaticFilesStorage'


# Media & GCS Configuration

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

if DEBUG == 0:
    DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
    GS_BUCKET_NAME = 'sihibah'
    GS_CREDENTIALS = service_account.Credentials.from_service_account_file("gloud-service-key.json")


# App logger configuration

ENABLE_LOGGING = int(os.environ.get('ENABLE_LOGGING', default=0))
if ENABLE_LOGGING:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    }


# Set ENABLE_MAHASISWA_LOGIN flag to True to allow Mahasiswa to login

ENABLE_MAHASISWA_LOGIN = int(os.environ.get('ENABLE_MAHASISWA_LOGIN', default=0))
