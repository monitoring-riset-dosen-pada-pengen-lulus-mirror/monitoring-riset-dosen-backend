from django.contrib.auth import get_user_model
from django.db import models


class Profil(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='profil')
    nama_lengkap = models.CharField(max_length=255, blank=True)
    email = models.EmailField(max_length=255, null=True, blank=True)

    def __str__(self) -> str:
        return self.nama_lengkap or str(self.user)


class Dosen(Profil):
    nip = models.CharField(max_length=12, blank=True)
    is_manajemen = models.BooleanField(default=False)


class TenagaKependidikan(Profil):
    nip = models.CharField(max_length=12, blank=True)


class Mahasiswa(Profil):
    npm = models.CharField(max_length=10, blank=True)


class AllowedAccount(models.Model):
    class TipeUser(models.TextChoices):
        DOSEN = "Dosen"
        TENAGA_KEPENDIDIKAN = "Tenaga Kependidikan"

    username = models.CharField(max_length=150, unique=True)
    nama_lengkap = models.CharField(max_length=255, blank=True)
    tipe_user = models.CharField(
        max_length=255,
        choices=TipeUser.choices,
        default=TipeUser.DOSEN
    )

    def __str__(self) -> str:
        return f'{self.nama_lengkap}'
