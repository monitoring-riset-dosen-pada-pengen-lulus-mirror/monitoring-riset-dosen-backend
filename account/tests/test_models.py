from django.contrib.auth.models import User
from django.test import TestCase

from account.models import Dosen, Mahasiswa, TenagaKependidikan


class DosenModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Dosen",
            last_name="A",
            email="dosen_a@gmail.com",
            username="dosen_a",
            password="password"
        )
        self.user_dosen = Dosen.objects.create(
            user=self.user,
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}',
            email="dosen_a@gmail.com",
            nip="123456789012",
            is_manajemen=False,
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Dosen
        model's __str__() is working properly
        """
        assert self.user_dosen.nama_lengkap == str(self.user_dosen)
        assert type(self.user_dosen.__str__()) == str


class TenagaKependidikanModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="TenagaKependidikan",
            last_name="A",
            email="tenaga_kependidikan_a@gmail.com",
            username="tenaga_kependidikan_a",
            password="password"
        )
        self.user_tenaga_kependidikan = TenagaKependidikan.objects.create(
            user=self.user,
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}',
            email="tenaga_kependidikan_a@gmail.com",
            nip="123456789012"
        )

    def test_string_representation(self):
        """
        Testing the case whether or not TenagaKependidikan
        model's __str__() is working properly
        """
        assert self.user_tenaga_kependidikan.nama_lengkap == str(self.user_tenaga_kependidikan)
        assert type(self.user_tenaga_kependidikan.__str__()) == str


class MahasiswaModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            first_name="Mahasiswa",
            last_name="A",
            email="mahasiswa_a@gmail.com",
            username="mahasiswa_a",
            password="password"
        )
        self.user_mahasiswa = Mahasiswa.objects.create(
            user=self.user,
            nama_lengkap=f'{self.user.first_name} {self.user.last_name}',
            email="mahasiswa_a@gmail.com",
            npm="1906123123"
        )

    def test_string_representation(self):
        """
        Testing the case whether or not Mahasiswa
        model's __str__() is working properly
        """
        assert self.user_mahasiswa.nama_lengkap == str(self.user_mahasiswa)
        assert type(self.user_mahasiswa.__str__()) == str
