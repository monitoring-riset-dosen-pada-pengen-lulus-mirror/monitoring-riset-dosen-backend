from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.test import RequestFactory, TestCase

from account.cas.services import AccountServices, CASV2AuthServices
from account.models import AllowedAccount, Dosen, Mahasiswa, TenagaKependidikan
from account.serializers import (
    DosenSerializer, MahasiswaSerializer, TenagaKependidikanSerializer,
)


class CAS2AuthServicesTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.username = 'mahasiswa1'
        self.attributes = {
            'ldap_cn': 'Mahasiswa 1',
            'kd_org': '55.55.55.55',
            'peran_user': 'staff',
            'nama': 'Mahasiswa 1',
            'npm': '1234567890'
        }

    def test_process(self):
        settings.ENABLE_MAHASISWA_LOGIN = True

        profil = CASV2AuthServices.process(
            username=self.username,
            attributes=self.attributes
        )

        self.assertEqual(profil['account']['user'], self.username)
        self.assertEqual(profil['account']['npm'], self.attributes['npm'])
        self.assertEqual(profil['account']['email'], f'{self.username}@ui.ac.id')
        self.assertEqual(profil['account']['nama_lengkap'], self.attributes['nama'])
        self.assertEqual(profil['role'], 'mahasiswa')
        self.assertNotEqual(profil['token'], None)

    def test_if_process_django_auth_user_successful(self):
        django_user_obj = CASV2AuthServices.process_django_auth_user(
            self,
            username=self.username,
            attributes=self.attributes
        )
        self.assertEqual(
            django_user_obj,
            User.objects.get(username=self.username)
        )

    def test_if_process_account_profil_successful(self):
        settings.ENABLE_MAHASISWA_LOGIN = True

        django_user_obj = CASV2AuthServices.process_django_auth_user(
            self,
            username=self.username,
            attributes=self.attributes
        )
        mahasiswa_obj = Mahasiswa.objects.create(
            user=django_user_obj,
            nama_lengkap=self.attributes['nama'],
            email=django_user_obj.email,
            npm=self.attributes['npm']
        )
        serialized_mahasiswa_data, _, _ = CASV2AuthServices.process_account_profil(
            self,
            django_user=django_user_obj,
            attributes=self.attributes
        )
        self.assertEqual(
            serialized_mahasiswa_data,
            MahasiswaSerializer(mahasiswa_obj).data
        )

    def test_if_add_jwt_token_successful(self):
        django_user_obj = CASV2AuthServices.process_django_auth_user(
            self,
            username=self.username,
            attributes=self.attributes
        )
        account_data, role, _ = CASV2AuthServices.process_account_profil(
            self,
            django_user=django_user_obj,
            attributes=self.attributes
        )
        tokenized_account_data = CASV2AuthServices.add_jwt_token(
            self,
            django_user=django_user_obj,
            account=account_data,
            role=role
        )
        self.assertEqual(tokenized_account_data['account'], account_data)
        self.assertEqual(tokenized_account_data['role'], 'mahasiswa')
        self.assertNotEqual(tokenized_account_data['token'], None)

    def test_if_mahasiswa_is_forbidden_to_log_in(self):
        settings.ENABLE_MAHASISWA_LOGIN = False

        self.assertRaises(
            PermissionDenied,
            CASV2AuthServices.process,
            self.username,
            self.attributes,
        )


class AccountServicesTest(TestCase):
    def setUp(self):
        self.username_mahasiswa = 'mahasiswa'
        self.attributes_mahasiswa = {
            'ldap_cn': 'Mahasiswa',
            'kd_org': '11.22.33.44.55',
            'peran_user': 'staff',
            'nama': 'Mahasiswa',
            'npm': '1234554321'
        }
        self.user_mahasiswa = User.objects.create_superuser(
            email="user_mahasiswa@ui.ac.id",
            username=self.username_mahasiswa,
            password="password"
        )
        self.obj_mahasiswa = Mahasiswa.objects.create(
            user=self.user_mahasiswa,
            nama_lengkap=self.attributes_mahasiswa['nama'],
            email=self.user_mahasiswa.email,
            npm=self.attributes_mahasiswa['npm']
        )

        self.username_dosen = 'dosen'
        self.attributes_dosen = {
            'ldap_cn': 'Dosen',
            'peran_user': 'staff',
            'nama': 'Dosen',
            'nip': '1234567890'
        }
        self.user_dosen = User.objects.create_superuser(
            email="user_dosen@ui.ac.id",
            username=self.username_dosen,
            password="password"
        )
        self.obj_dosen = Dosen.objects.create(
            user=self.user_dosen,
            nama_lengkap=self.attributes_dosen['nama'],
            email=self.user_dosen.email,
            nip=self.attributes_dosen['nip'],
            is_manajemen=True
        )
        AllowedAccount.objects.create(
            username=self.username_dosen,
            tipe_user=AllowedAccount.TipeUser.DOSEN
        )

        self.username_tenaga_kependidikan = 'tenaga_kependidikan'
        self.attributes_tenaga_kependidikan = {
            'ldap_cn': 'Tenaga Kependidikan',
            'peran_user': 'staff',
            'nama': 'Tenaga Kependidikan',
            'nip': '0987654321'
        }
        self.user_tenaga_kependidikan = User.objects.create_superuser(
            email="user_tenaga_kependidikan@ui.ac.id",
            username=self.username_tenaga_kependidikan,
            password="password"
        )
        self.obj_tenaga_kependidikan = TenagaKependidikan.objects.create(
            user=self.user_tenaga_kependidikan,
            nama_lengkap=self.attributes_tenaga_kependidikan['nama'],
            email=self.user_tenaga_kependidikan.email,
            nip=self.attributes_tenaga_kependidikan['nip'],
        )
        AllowedAccount.objects.create(
            username=self.username_tenaga_kependidikan,
            tipe_user=AllowedAccount.TipeUser.TENAGA_KEPENDIDIKAN
        )

    def test_process_account_mahasiswa(self):
        settings.ENABLE_MAHASISWA_LOGIN = True

        account_tuple = AccountServices.process_account(
            self.user_mahasiswa,
            self.attributes_mahasiswa
        )

        self.assertEqual(
            account_tuple,
            (
                MahasiswaSerializer(self.obj_mahasiswa).data,
                'mahasiswa',
                True
            )
        )

    def test_process_account_dosen(self):
        account_tuple = AccountServices.process_account(
            self.user_dosen,
            self.attributes_dosen
        )

        self.assertEqual(
            account_tuple,
            (
                DosenSerializer(self.obj_dosen).data,
                'dosen',
                True
            )
        )

    def test_process_account_tenaga_kependidikan(self):
        account_tuple = AccountServices.process_account(
            self.user_tenaga_kependidikan,
            self.attributes_tenaga_kependidikan
        )

        self.assertEqual(
            account_tuple,
            (
                TenagaKependidikanSerializer(self.obj_tenaga_kependidikan).data,
                'tenaga_kependidikan',
                True
            )
        )
