from django.conf import settings
from django.test import TestCase

from account.cas.cas import CASClient
from account.cas.utils import get_cas_client


class CASUtilsTest(TestCase):
    def setUp(self):
        self.server_url = 'https://sso.ui.ac.id/cas2/'
        self.service_url = 'http://localhost:3000/'
        self.cas_client_v2 = CASClient(service_url=self.service_url,
                                       server_url=self.server_url,
                                       version=2)

    def test_if_get_cas_utils_successfully_returns_cas_client_v2(self):
        settings.CAS_SERVER_URL = 'https://sso.ui.ac.id/cas2/'
        settings.CAS_VERSION = 2

        cas_client = get_cas_client(self.service_url)

        self.assertEqual(
            cas_client.__class__,
            self.cas_client_v2.__class__
        )
