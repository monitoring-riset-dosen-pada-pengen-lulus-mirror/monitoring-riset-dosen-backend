from rest_framework.test import APIRequestFactory, APITestCase

from account.cas.cas import CASClient


class SSOLoginViewTest(APITestCase):
    def setUp(self):
        self.request_factory = APIRequestFactory()
        self.server_url = 'https://sso.ui.ac.id/cas2/'
        self.service_url = 'http://localhost:3000/'
        self.ticket = 'ST-91-abcdefghijklmn-sso.ui.ac.id'
        self.cas_client_v2 = CASClient(service_url=self.service_url,
                                       server_url=self.server_url,
                                       version=2)
        self.username = 'mahasiswa1'
        self.attributes = {
            'ldap_cn': 'Mahasiswa 1',
            'kd_org': '55.55.55.55',
            'peran_user': 'staff',
            'nama': 'Mahasiswa 1',
            'npm': '1234567890'
        }

    def test_dummy_ticket_will_return_400_bad_request(self):
        payload = {'ticket': self.ticket, 'service_url': self.service_url}
        response = self.client.post(
            '/api/account/login',
            payload,
            format='json'
        )
        self.assertEqual(response.status_code, 400)

    def test_incomplete_payload_will_return_400_bad_request(self):
        payload = {}
        response = self.client.post(
            '/api/account/login',
            payload,
            format='json'
        )
        self.assertEqual(response.status_code, 400)
