import json
from http import HTTPStatus

from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from account.cas.services import CASV2AuthServices
from account.cas.utils import get_cas_client
from account.swagger_params import sso_login_view_docs


class SSOLoginView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        request_body=sso_login_view_docs['request_body'],
        responses=sso_login_view_docs['responses']
    )
    def post(self, request):
        request_body = json.loads(request.body.decode())
        ticket = request_body.get('ticket', None)
        service_url = request_body.get('service_url', None)

        if (ticket is None) or (service_url is None):
            return JsonResponse(data={}, status=HTTPStatus.BAD_REQUEST)

        client = get_cas_client(service_url)
        user, attributes, _ = client.verify_ticket(ticket=ticket)

        if attributes is None:
            return JsonResponse(data={}, status=HTTPStatus.BAD_REQUEST)

        profil = None
        try:
            profil = CASV2AuthServices.process(
                username=user,
                attributes=attributes
            )
        except PermissionDenied:
            return JsonResponse(
                data={
                    'message': f'''
                        User {str(user)} tidak bisa masuk karena bukan termasuk peran
                        yang dibolehkan. Kontak Tim Manajemen Riset Fakultas untuk informasi lebih lanjut.
                    '''.strip()
                },
                safe=False,
                status=HTTPStatus.FORBIDDEN
            )

        return JsonResponse(data=profil, status=HTTPStatus.OK)
