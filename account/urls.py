import django_cas_ng.views
from django.urls import path

from . import views

app_name = 'account'
urlpatterns = [
    path('login', views.SSOLoginView.as_view(), name='login'),
    path('cas-ng/login', django_cas_ng.views.LoginView.as_view(), name='cas_ng_login'),
    path('cas-ng/logout', django_cas_ng.views.LogoutView.as_view(), name='cas_ng_logout'),
]
