blocked_models = [
    'django.contrib.admin.models.LogEntry',
    'django.contrib.auth.models.Permission',
    'django.contrib.contenttypes.models.ContentType',
    'django.contrib.sessions.models.Session',
    'django_cas_ng.models.ProxyGrantingTicket',
    'django_cas_ng.models.SessionTicket',
    'account.models.AllowedAccount'
]
