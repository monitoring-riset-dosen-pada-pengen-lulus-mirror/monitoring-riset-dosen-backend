from rest_framework_simplejwt.tokens import RefreshToken


def get_tokens_for_user(user):
    """
    Create Token based on Django User

    Reference:
    https://django-rest-framework-simplejwt.readthedocs.io/en/latest/creating_tokens_manually.html
    """
    refresh = RefreshToken.for_user(user)
    return str(refresh.access_token), str(refresh)
