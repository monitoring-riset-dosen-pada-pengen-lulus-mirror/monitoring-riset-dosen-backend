from django.contrib.auth.models import User
from rest_framework import serializers

from account.models import Dosen, Mahasiswa, Profil, TenagaKependidikan


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def to_representation(self, instance):
        return instance.username


class ProfilSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Profil
        fields = '__all__'


class MahasiswaSerializer(ProfilSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Mahasiswa
        fields = '__all__'


class DosenSerializer(ProfilSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Dosen
        fields = '__all__'


class TenagaKependidikanSerializer(ProfilSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = TenagaKependidikan
        fields = '__all__'
