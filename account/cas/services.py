from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied

from account.cas.utils import process_sso_name
from account.models import AllowedAccount, Dosen, Mahasiswa, TenagaKependidikan
from account.serializers import (
    DosenSerializer, MahasiswaSerializer, TenagaKependidikanSerializer,
)
from account.utils import get_tokens_for_user


class CASV2AuthServices:
    """
    A set of services made to process the auth data given
    from SSO UI. Only works for CAS Client Version 2.
    """

    @classmethod
    def process(cls, username, attributes):
        django_user = cls.process_django_auth_user(cls, username, attributes)
        account, role, is_success = cls.process_account_profil(cls, django_user, attributes)
        if not is_success:
            raise PermissionDenied()
        return cls.add_jwt_token(cls, django_user, account, role)

    def process_django_auth_user(self, username, attributes):
        """Create or update Django's Auth User model with SSO attributes."""
        user, created = User.objects.get_or_create(
            username=username
        )

        if user or created:
            first_name, last_name = process_sso_name(attributes.get('nama', ''))

            email = ''
            if username is not None:
                email = username + (
                    '@cs.ui.ac.id'
                    if 'nip' in attributes.keys()
                    else '@ui.ac.id'
                )

            user.first_name = first_name or user.first_name
            user.last_name = last_name or user.last_name
            user.email = email
            user.is_active = True
            user.is_staff = True
            user.save()

        return user

    def process_account_profil(self, django_user: User, attributes):
        """
        Create or update Dosen/TenagaKependidikan/Mahasiswa objects
        based on its SSO role.
        """
        return AccountServices.process_account(django_user, attributes)

    def add_jwt_token(self, django_user, account, role):
        """
        Append token to the serialized Dosen/TenagaKependidikan/Mahasiswa data.
        Tokens are returned in the format of `initial_token` and `refresh_token`.
        """
        access, refresh = get_tokens_for_user(django_user)

        return {
            'refresh': refresh,
            'token': access,
            'account': account,
            'role': role,
            'is_admin': django_user.is_superuser,
        }


class AccountServices:
    """
    A set of services to handle the conversion of SSO
    auth data to Dosen/TenagaKependidikan model object.

    Mahasiswa model is added for development purpose
    """

    @classmethod
    def process_account(cls, django_user, attributes):
        username = django_user.username
        params = [django_user, attributes]

        if settings.ENABLE_MAHASISWA_LOGIN and 'npm' in attributes.keys():
            return cls.process_mahasiswa(cls, *params)

        if AllowedAccount.objects.filter(
            username=username,
            tipe_user=AllowedAccount.TipeUser.DOSEN
        ).exists():
            return cls.process_dosen(cls, *params)

        if AllowedAccount.objects.filter(
            username=username,
            tipe_user=AllowedAccount.TipeUser.TENAGA_KEPENDIDIKAN
        ).exists():
            return cls.process_tenaga_kependidikan(cls, *params)

        return None, None, False

    def process_mahasiswa(_, django_user: User, attributes):
        """Get or create logged in User as Mahasiswa"""
        nama_lengkap = attributes.get('nama', '')
        npm = attributes.get('npm', '')

        mahasiswa_obj, _ = Mahasiswa.objects.get_or_create(
            user=django_user,
            nama_lengkap=nama_lengkap,
            email=django_user.email,
            npm=npm
        )

        return (
            MahasiswaSerializer(mahasiswa_obj).data,
            'mahasiswa',
            True
        )

    def process_dosen(_, django_user: User, attributes):
        """Get or create logged in User as Dosen"""
        nama_lengkap = attributes.get('nama', '')
        nip = attributes.get('nip', '')

        dosen_obj, _ = Dosen.objects.get_or_create(
            user=django_user,
            nama_lengkap=nama_lengkap,
            email=django_user.email,
            is_manajemen=True,
            nip=nip
        )

        return (
            DosenSerializer(dosen_obj).data,
            'dosen',
            True
        )

    def process_tenaga_kependidikan(_, django_user: User, attributes):
        """Get or create logged in User as Tenaga Kependidikan"""
        nama_lengkap = attributes.get('nama', '')
        nip = attributes.get('nip', '')

        tenaga_kependidikan_obj, _ = TenagaKependidikan.objects.get_or_create(
            user=django_user,
            nama_lengkap=nama_lengkap,
            email=django_user.email,
            nip=nip
        )

        return (
            TenagaKependidikanSerializer(tenaga_kependidikan_obj).data,
            'tenaga_kependidikan',
            True
        )
