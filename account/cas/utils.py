from typing import Tuple

from django.conf import settings as django_settings

from .cas import CASClient


def get_cas_client(
    service_url=None,
    server_url=django_settings.CAS_SERVER_URL,
    request=None
) -> CASClient:
    """
    initializes the CASClient according to
    the CAS_* settigs
    """
    # Handle CAS_SERVER_URL without protocol and hostname
    if server_url and request and server_url.startswith('/'):
        scheme = request.META.get("X-Forwarded-Proto", request.scheme)
        server_url = scheme + "://" + request.META['HTTP_HOST'] + server_url

    return CASClient(service_url=service_url, server_url=server_url, version=2)


def process_sso_name(full_name: str) -> Tuple[str, str]:
    """Extract First Name and Last Name from Full Name"""
    splitted = full_name.split()
    first_name = splitted[0]
    last_name = ' '.join(splitted[1:])

    return first_name, last_name
