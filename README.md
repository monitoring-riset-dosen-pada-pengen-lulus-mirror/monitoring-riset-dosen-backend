[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/monitoring-riset-dosen/sihibah-be/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/monitoring-riset-dosen/sihibah-be/commits/staging)
[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/monitoring-riset-dosen/sihibah-be/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/monitoring-riset-dosen/sihibah-be/commits/staging)

## URL Penting

- Website staging: http://shiba-be-staging.herokuapp.com/
- Sonarqube: https://pmpl.cs.ui.ac.id/sonarqube/dashboard?branch=staging&id=ppl-fasilkom-ui_2022_kelas-c_monitoring-riset-dosen_sihibah-be_AX8XknCvmTzPxwcer_j7

## Tutorial Instalasi untuk _Development_

Prasyarat:

- [Docker sudah terinstall](https://docs.docker.com/get-docker/)
- [Docker Compose sudah terinstall](https://docs.docker.com/compose/install/)

Langkah-langkah instalasi:

1. Jalankan instruksi ini di terminal _operating system_ Anda 
```shell
docker-compose -f docker-compose-dev.yml up -d --build
```
2. Anda bisa langsung men-_develop_ aplikasi ini

## Tutorial _Deployment_

Beberapa informasi yang perlu diketahui sebelum melakukan _deployment_:

- Aplikasi ini menggunakan [Docker Container](https://www.docker.com/) untuk _deployment_.
- Aplikasi ini menggunakan [Google Cloud Storage](https://cloud.google.com/storage) untuk menyimpan berkas.
- Aplikasi ini menggunakan [PostgreSQL](https://www.postgresql.org/) sebagai database.

Untuk melakukan _deployment_, yang perlu dilakukan hanyalah:

1. Buat file dengan nama **gloud-service-key.json** yang mana merupakan [service account Google Cloud Platform](https://cloud.google.com/iam/docs/service-accounts) key dengan izin CRUD terhadap Google Cloud Storage
2. Simpan **gloud-service-key.json** di dalam directory utama back end SIHIBAH
3. Lakukan konfigurasi keamanan CORS dan CSRF pada bagian ini di sihibah/settings.py (lihat gambar di bawah)

![image](https://i.postimg.cc/634YtMcB/temp.png)

4. Jalankan instruksi ini untuk mem-build Docker Image: `docker build -t <nama_image> .` 
5. Lakukan container _deployment_ sesuai dengan server provider

Ketika ingin melakukan _deployment_, maka perhatikan [environment variable Docker Image](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file) berikut sebelum menjalankannya:

| Nama Variable | Deskripsi | Default |
| ------ | ------ | ------ |
| PG_USER | Nama user dari Postgresql | postgres | 
| PG_HOST | Ip address atau URL dari Postgresql | postgres |
| PG_PORT | Port dari Postgresql tersebut | 5432 |
| PG_PASSWORD | Password dari Postgresql | Admin123* |
| PG_NAME | Nama dari _database_ Postgresql | postgres |
| PORT | _Port_ untuk mengakses back end SIHIBAH | _blank_ _string_ |


 
